#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QtNetwork>
#include <QtWidgets>

class TcpServer : public QTcpServer
{
    Q_OBJECT

public:
    TcpServer(QObject *parent = 0) : QTcpServer(parent)
    {
        connect(this, SIGNAL(newConnection()), SLOT(slotNewConnection()));
    }

public slots:
    void slotNewConnection()
    {
        connection = nextPendingConnection();
        connect(connection, SIGNAL(readyRead()), SLOT(readData()));

        QMessageLogger().info(QString("Connection created on ").append(QString::number(connection->peerPort())).toUtf8().data());
    }

    void readData()
    {
        emit receivedData(QString(connection->readAll()));
    }

signals:
    void receivedData(const QString& data);

private:
    QTcpSocket* connection;
};

#endif // TCPSERVER_H
