#ifndef TCPBACKGROUNDCONNECTION_H
#define TCPBACKGROUNDCONNECTION_H

#include <QObject>
#include <QThread>
#include "tcpserver.h"

class TcpBackgroundConnection : public QObject
{
    Q_OBJECT
public:
    TcpBackgroundConnection(QObject *parent, const QString& address, const QString& port) : QObject(parent)
    {
        server = new TcpServer();
        server->setParent(this);
        this->address = address;
        this->port = port;
        active = false;
        connect(server, &TcpServer::receivedData, [this](const QString& data){
            emit receivedData(data);
        });
    }

public:
    bool start()
    {
        if(!server->listen(QHostAddress(address), port.toUInt()))
        {
            QMessageLogger().warning(QString("[WARNING] Error listening on port ").append(port).toUtf8().data());
            return false;
        }
        active = true;
        return true;
    }

    void stop()
    {/*
        if(server != NULL)
        {
            if(server->isListening())
            {
                server->close();
            }
        }
        if(active)
        {
            server->close();
        }*/
    }

private:
    TcpServer* server;
    QString address;
    QString port;
    bool active;

signals:
    void receivedData(const QString& data);
};

#endif // TCPBACKGROUNDCONNECTION_H
