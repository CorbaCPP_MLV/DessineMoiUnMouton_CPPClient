#include "newimagedialog.h"
#include "ui_newimagedialog.h"

NewImageDialog::NewImageDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewImageDialog)
{
    ui->setupUi(this);
}

NewImageDialog::~NewImageDialog()
{
    delete ui;
}

void NewImageDialog::on_buttonBox_accepted()
{
    emit imageCreated(ui->widthSpinBox->value(), ui->heightSpinBox->value());
}

void NewImageDialog::on_buttonBox_rejected()
{
    close();
}
