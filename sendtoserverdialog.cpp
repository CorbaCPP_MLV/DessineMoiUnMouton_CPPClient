#include "sendtoserverdialog.h"
#include "ui_sendtoserverdialog.h"

SendToServerDialog::SendToServerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SendToServerDialog)
{
    ui->setupUi(this);
}

SendToServerDialog::~SendToServerDialog()
{
    delete ui;
}

void SendToServerDialog::setData(QByteArray& data, unsigned width, unsigned height, dessine_module::HostInfo& info)
{
    QMessageLogger().info(QString("[INFO] Set ").append(QString::number(data.size())).append(" bytes").toUtf8().data());
    this->data = QByteArray(data);
    this->imageHeight = height;
    this->imageWidth = width;
    this->info = info;
}

void SendToServerDialog::on_sendPushButton_clicked()
{
    if(!connectToServer())
    {
        return;
    }

    QMessageLogger().info(QString("[INFO] Sending ").append(QString::number(data.size())).append(" bytes").toUtf8().data());
    dessine_module::Image im = ConnectionUtils::XMLImageToCORBAImage(data, imageWidth, imageHeight);
    if(serverInstance != nullptr)
    {
        try
        {
            char* theTicket = serverInstance->pushImage(im, info);

            QMessageBox::information(this, tr("Dessine"),
                                  tr("Image correctly sent to server."),
                                  QMessageBox::Ok);

            //if the communication succeeded, emit a signal
            dessine_module::HostInfo serverInfo;
            serverInfo.ipAddress = CORBA::string_dup(ui->addressLineEdit->text().toStdString().data());
            serverInfo.port = CORBA::string_dup(ui->portLineEdit->text().toStdString().data());

            emit imagePushed(data, QString(theTicket), serverInfo);
        }
        catch(CORBA::TRANSIENT&)
        {
            QMessageLogger().critical("Caught system exception TRANSIENT -- unable to contact the server.");
            QMessageBox::critical(this, tr("Error"),
                                  tr("Unable to contact server. Check the server status."),
                                  QMessageBox::Ok);
        }
    }
    else
    {
        QMessageBox::critical(this, tr("Error"),
                              tr("Unable to contact server. Check the server status."),
                              QMessageBox::Ok);
    }
}

bool SendToServerDialog::connectToServer()
{
    QHostAddress myIP;

    //Use the QHostAddress's setAddress method to check if user has entered a correct IP address
    if(myIP.setAddress(ui->addressLineEdit->text()))
    {
        serverInstance = ConnectionUtils::getConnection(ui->addressLineEdit->text(), ui->portLineEdit->text());
        return true;
    }
    else
    {
        QMessageBox::critical(this, tr("Error"),
                              tr("Invalid IP address."),
                              QMessageBox::Ok);
        return false;
    }
}

void SendToServerDialog::on_closePushButton_clicked()
{
    close();
}
