#ifndef COMMENTSDIALOG_H
#define COMMENTSDIALOG_H

#include <QDialog>
#include <QStandardItem>
#include "CORBA/connectionutils.h"
#include "CORBA/orb_interface.hh"
#include "commentsholder.h"
#include "renderarea.h"

namespace Ui {
class CommentsDialog;
}

class CommentsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CommentsDialog(QWidget *parent = 0);
    ~CommentsDialog();

    void getComments(const QList<QString>&tickets, const QString &address, const QString &port);
    void setCommentsHolder(CommentsHolder* holder);

private slots:
    void on_buttonBox_rejected();

    void on_commentsTreeView_clicked(const QModelIndex &index);

private:
    CommentsHolder* holder;
    QSharedPointer<RenderArea> renderArea;
    QSharedPointer<QStandardItemModel> treeViewDataModel;
    Ui::CommentsDialog *ui;
};

#endif // COMMENTSDIALOG_H
