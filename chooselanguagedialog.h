#ifndef CHOOSELANGUAGEDIALOG_H
#define CHOOSELANGUAGEDIALOG_H

#include <QDialog>

namespace Ui {
class ChooseLanguageDialog;
}

class ChooseLanguageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChooseLanguageDialog(QWidget *parent = 0);
    ~ChooseLanguageDialog();

private slots:
    void on_en_radioButton_clicked();

    void on_fr_radioButton_clicked();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

public:
    void setLanguage(const QString& language);


private:
    Ui::ChooseLanguageDialog *ui;

signals:
    void onLanguageChoosed(const QString locale);
};

#endif // CHOOSELANGUAGEDIALOG_H
