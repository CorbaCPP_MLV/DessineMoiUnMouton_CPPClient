#ifndef NEWIMAGEDIALOG_H
#define NEWIMAGEDIALOG_H

#include <QDialog>

namespace Ui {
class NewImageDialog;
}

class NewImageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewImageDialog(QWidget *parent = 0);
    ~NewImageDialog();

signals:
    void imageCreated(const unsigned width, const unsigned height);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();



private:
    Ui::NewImageDialog *ui;
};

#endif // NEWIMAGEDIALOG_H
