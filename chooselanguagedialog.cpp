#include "chooselanguagedialog.h"
#include "ui_chooselanguagedialog.h"

ChooseLanguageDialog::ChooseLanguageDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChooseLanguageDialog)
{
    ui->setupUi(this);

    QString locale(QLocale::system().name());
    locale.remove(0, locale.indexOf('_') + 1);
    if(locale == "FR")
    {
        ui->fr_radioButton->setChecked(true);
    }
    else
    {
        ui->en_radioButton->setChecked(true);
    }
}

ChooseLanguageDialog::~ChooseLanguageDialog()
{
    delete ui;
}

void ChooseLanguageDialog::setLanguage(const QString &language)
{
    if(language == "FR")
    {
        ui->fr_radioButton->setChecked(true);
    }
    else
    {
        ui->en_radioButton->setChecked(true);
    }
}

void ChooseLanguageDialog::on_en_radioButton_clicked()
{
    ui->fr_radioButton->setChecked(false);
}

void ChooseLanguageDialog::on_fr_radioButton_clicked()
{
    ui->en_radioButton->setChecked(false);
}

void ChooseLanguageDialog::on_buttonBox_accepted()
{
    if(ui->fr_radioButton->isChecked())
    {
        onLanguageChoosed("FR");
    }
    else
    {
        onLanguageChoosed("EN");
    }
    close();
}

void ChooseLanguageDialog::on_buttonBox_rejected()
{
    close();
}
