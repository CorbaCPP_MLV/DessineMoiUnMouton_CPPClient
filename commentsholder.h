#ifndef COMMENTSHOLDER_H
#define COMMENTSHOLDER_H

#include <QByteArray>
#include <QStringList>
#include <QMap>

class CommentsHolder
{

private:
    struct commentsValue
    {
        QByteArray imageData;
        QStringList comments;
    };

    QMap<QString, commentsValue> map;

public:
    CommentsHolder();
    void registerImage(const QString& ticket, const QByteArray& image);
    void registerComments(const QString& ticket, const QStringList& comments);
    QByteArray getImage(const QString& ticket);
    QStringList getComments(const QString& ticket);
    QStringList getTickets();
};

#endif // COMMENTSHOLDER_H
