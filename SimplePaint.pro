#-------------------------------------------------
#
# Project created by QtCreator 2017-01-22T10:48:41
#
#-------------------------------------------------

QT       += core gui network
LIBS += -lboost_system -lboost_signals
LIBS += -lomniORB4 -lomnithread -lomniDynamic4
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = SimplePaint
TEMPLATE = app
TRANSLATIONS = simplepaint_fr.ts simplepaint_en.ts

SOURCES += main.cpp\
    renderarea.cpp \
    geometrie/cercle.cpp \
    geometrie/image.cpp \
    geometrie/ligne.cpp \
    geometrie/rectangle.cpp \
    geometrie/ellipse.cpp \
    mainwindow.cpp \
    geometrie/convexpolygon.cpp \
    newimagedialog.cpp \
    sendtoserverdialog.cpp \
    CORBA/orb_interfaceSK.cc \
    CORBA/dessine_module_dessine_i.cpp \
    CORBA/dessine_module_communication_i.cpp \
    CORBA/connection.cpp \
    CORBA/connectionutils.cpp \
    CORBA/backgroundconnection.cpp \
    CORBA/omnimapperproxy.cpp \
    chooselanguagedialog.cpp \
    Network/tcpbackgroundconnection.cpp \
    Network/tcpserver.cpp \
    localserverconfigdialog.cpp \
    commentsdialog.cpp \
    commentsholder.cpp \
    aboutdialog.cpp

HEADERS  += \
    renderarea.h \
    geometrie/cercle.h \
    geometrie/figure.h \
    geometrie/image.h \
    geometrie/ligne.h \
    geometrie/rectangle.h \
    geometrie/ellipse.h \
    mainwindow.h \
    geometrie/convexpolygon.h \
    newimagedialog.h \
    geometrie/serializationutils.h \
    sendtoserverdialog.h \
    CORBA/orb_interface.hh \
    CORBA/dessine_module_dessine_i.h \
    CORBA/dessine_module_communication_i.h \
    CORBA/connection.h \
    CORBA/connectionutils.h \
    CORBA/backgroundconnection.h \
    CORBA/omnimapperproxy.h \
    chooselanguagedialog.h \
    Network/tcpbackgroundconnection.h \
    Network/tcpserver.h \
    localserverconfigdialog.h \
    commentsdialog.h \
    commentsholder.h \
    aboutdialog.h

FORMS    += \
    mainwindow.ui \
    newimagedialog.ui \
    sendtoserverdialog.ui \
    chooselanguagedialog.ui \
    localserverconfigdialog.ui \
    commentsdialog.ui \
    aboutdialog.ui

DISTFILES += \
    CORBA/orb_interface.idl \
    simplepaint_fr.ts \
    simplepaint_en.ts

RESOURCES += \
    resources.qrc
