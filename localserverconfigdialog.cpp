#include "localserverconfigdialog.h"
#include "ui_localserverconfigdialog.h"

LocalServerConfigDialog::LocalServerConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LocalServerConfigDialog)
{
    ui->setupUi(this);
}

LocalServerConfigDialog::~LocalServerConfigDialog()
{
    delete ui;
}

void LocalServerConfigDialog::setPort(const QString& val)
{
    ui->portLineEdit->setText(val);
}

void LocalServerConfigDialog::setUseLocalhost(const bool val)
{
    ui->useLocalhostAddressCheckBox->setChecked(val);
}

void LocalServerConfigDialog::on_buttonBox_accepted()
{
    emit configured(ui->portLineEdit->text(), ui->useLocalhostAddressCheckBox->isChecked());
    close();
}

void LocalServerConfigDialog::on_buttonBox_rejected()
{
    close();
}
