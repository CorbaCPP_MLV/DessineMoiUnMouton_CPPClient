#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "geometrie/cercle.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    useLocalhostAddress = true;
    savedChanges = false;
    translator = new QTranslator();

    ui->scrollArea->setBackgroundRole(QPalette::Dark);
    ui->brushColorPushButton->setStyleSheet("background-color: rgb(0,0,0);");

    setupRenderArea(QSharedPointer<RenderArea>(new RenderArea));
    setLanguage();

    hostInfo = ConnectionUtils::getHostInfo(useLocalhostAddress);
    startBackgroundTCPServer(QString(hostInfo.ipAddress), QString(hostInfo.port));

    connect(ui->figureListWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));
    connect(ui->figureListWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(on_FigureSelected(QListWidgetItem*)));

    //Set the line as default tool
    on_linePushButton_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startBackgroundTCPServer(const QString& address, const QString& port)
{
    QMessageLogger().info(QString("[INFO] Starting TCP server @").append(address).append(":").append(port).toUtf8().data());

    //Create a new connection using the specified port and the current machine IP address
    tcpServer = new TcpBackgroundConnection(this, address, port);

    //Handle the received data
    connect(tcpServer, &TcpBackgroundConnection::receivedData, this, &MainWindow::receiveData);

    //Start the server
    if(!tcpServer->start())
    {
        ui->statusbar->showMessage("Attention! Error listening on default port 1234. Please select another port.", 10000);
    }
}



void MainWindow::receiveData(const QString& ticket)
{
    QMessageLogger().info(QString("[INFO] Received ticket: ").append(ticket).toUtf8().data());
    ui->statusbar->showMessage("Received comment from server.", 10000);
    avaibleTickets.append(ticket);


}

void MainWindow::setLanguage()
{
    applicationLanguage = QLocale::system().name();
    applicationLanguage.remove(0, applicationLanguage.indexOf('_') + 1);
    changeLanguage(applicationLanguage);
}

void MainWindow::setupRenderArea(QSharedPointer<RenderArea> r, const unsigned width, const unsigned height)
{
    renderArea = r;
    ui->scrollArea->setWidget(renderArea.data());
    ui->scrollArea->setWidgetResizable(false);
    renderArea->setFixedWidth(width);
    renderArea->setFixedHeight(height);
    connect(renderArea.data(), SIGNAL(mousePressed(QPoint)), this, SLOT(onMousePressEvent(QPoint)));
    connect(renderArea.data(), SIGNAL(mouseReleased(QPoint)), this, SLOT(onMouseReleaseEvent(QPoint)));
    connect(renderArea.data(), SIGNAL(figureAdded(geometry::Figure&, int)), this, SLOT(on_FigureAdded(geometry::Figure&, int)));
}

void MainWindow::closeEvent(QCloseEvent */*event*/)
{
    quit();
}

void MainWindow::quit()
{
    if(!savedChanges)
    {
        if(askForSave())
        {
            saveImage();
        }
    }
    QApplication::quit();
}

void MainWindow::on_FigureSelected(QListWidgetItem* item)
{
    int selectedItemsCount = ui->figureListWidget->selectedItems().size();
    ui->deselectFiguresPushButton->setEnabled(selectedItemsCount > 0);

    if(selectedItemsCount > 0)
    {
        int figureID = item->data(Qt::UserRole).toInt();
        renderArea->highlightFigure(figureID);
    }
    else
    {
        renderArea->undoHighlight();
        ui->figureListWidget->clearSelection();
        ui->deselectFiguresPushButton->setEnabled(false);
    }
}

void MainWindow::showContextMenu(const QPoint& pos)
{
    // Handle global position
    QPoint globalPos = ui->figureListWidget->mapToGlobal(pos);

    // Create menu
    QMenu myMenu;
    myMenu.addAction(tr("Delete"),  this, SLOT(removeFigure()));

    QMenu sortByMenu;
    sortByMenu.setTitle(tr("Sort By"));


    QAction* sortByAscendingArea = new QAction(tr("Area"), this);
    QAction* sortByDescendingArea = new QAction(tr("Area (Descending)"), this);
    QAction* sortByAscendingPerimeter = new QAction(tr("Perimeter"), this);
    QAction* sortByDescendingPerimeter = new QAction(tr("Perimeter (Descending)"), this);
    QAction* sortByFartherDistanceFromOrigin = new QAction(tr("Distance from Origin"), this);
    QAction* sortByNearestDistanceFromOrigin = new QAction(tr("Distance from Origin (nearest)"), this);


    connect(sortByAscendingArea,  &QAction::triggered, this, [this] { sortFiguresByArea(true); });
    connect(sortByDescendingArea,  &QAction::triggered, this, [this] { sortFiguresByArea(false); });
    connect(sortByAscendingPerimeter,  &QAction::triggered, this, [this] { sortFiguresByPerimeter(true); });
    connect(sortByDescendingPerimeter,  &QAction::triggered, this, [this] { sortFiguresByPerimeter(false); });
    connect(sortByFartherDistanceFromOrigin,  &QAction::triggered, this, [this] { sortFiguresByDistanceFromOrigin(true); });
    connect(sortByNearestDistanceFromOrigin,  &QAction::triggered, this, [this] { sortFiguresByDistanceFromOrigin(false); });

    sortByMenu.addAction(sortByAscendingArea);
    sortByMenu.addAction(sortByDescendingArea);
    sortByMenu.addAction(sortByAscendingPerimeter);
    sortByMenu.addAction(sortByDescendingPerimeter);
    sortByMenu.addAction(sortByFartherDistanceFromOrigin);
    sortByMenu.addAction(sortByNearestDistanceFromOrigin);
    myMenu.addMenu(&sortByMenu);

    // Show context menu at handling position
    myMenu.exec(globalPos);
}

void MainWindow::sortFiguresByArea(bool ascending)
{
    sortFigures(ascending, 0);
}

void MainWindow::sortFiguresByPerimeter(bool ascending)
{
    sortFigures(ascending, 1);
}

void MainWindow::sortFiguresByDistanceFromOrigin(bool farther)
{
    sortFigures(farther, 2);
}

void MainWindow::sortFigures(bool order, unsigned parameter)
{
    //Sort the hash map keys (that contains the figures) according to their corresponding figure's area
    QList<unsigned> keys = renderArea->getImage()->getFigures().keys();
    sort(keys.begin(), keys.end(),
         [this, order, parameter](const unsigned & a, const unsigned & b)
    {
        if(parameter == 0)
        {
            if(order)
            {
                return renderArea->getImage()->getFigures()[a]->surface() > renderArea->getImage()->getFigures()[b]->surface();
            }
            return renderArea->getImage()->getFigures()[a]->surface() <= renderArea->getImage()->getFigures()[b]->surface();
        }
        else if(parameter == 1)
        {
            if(order)
            {
                return renderArea->getImage()->getFigures()[a]->perimeter() > renderArea->getImage()->getFigures()[b]->perimeter();
            }
            return renderArea->getImage()->getFigures()[a]->perimeter() <= renderArea->getImage()->getFigures()[b]->perimeter();
        }
        else if(parameter == 2)
        {
            if(order)
            {
                return renderArea->getImage()->getFigures()[a]->distanceFromOrigin() > renderArea->getImage()->getFigures()[b]->distanceFromOrigin();
            }
            return renderArea->getImage()->getFigures()[a]->distanceFromOrigin() <= renderArea->getImage()->getFigures()[b]->distanceFromOrigin();
        }
    });


    //Clear the selection of the list
    renderArea->undoHighlight();
    ui->figureListWidget->clearSelection();
    ui->deselectFiguresPushButton->setEnabled(false);
    ui->figureListWidget->clear();

    //Add the ordered figures to the list
    for(int i = 0; i < keys.size(); i++)
    {
        on_FigureAdded(*(renderArea->getImage()->getFigures()[keys[i]]), keys[i]);
    }
}

void MainWindow::removeFigure()
{
    // If multiple selection is on, we need to erase all selected items
    unsigned selectedItemsCount = ui->figureListWidget->selectedItems().size();
    QMessageLogger().info(QString("[INFO] Deleting ").append(QString::number(selectedItemsCount)).append(" figures").toUtf8().data());

    for(const auto& item : ui->figureListWidget->selectedItems())
    {
        int figureID = item->data(Qt::UserRole).toInt();
        QMessageLogger().info(QString("[INFO] Deleting figure with ID: ").append(QString::number(figureID)).toUtf8().data());

        renderArea->removeFigure(figureID);

        // And remove it
        delete item;
    }

    renderArea->undoHighlight();
    ui->figureListWidget->clearSelection();
    ui->deselectFiguresPushButton->setEnabled(false);
}

void MainWindow::on_FigureAdded(geometry::Figure& f, int figureID)
{
    QListWidgetItem* item = new QListWidgetItem;
    //Add the ID of the figure as argument for the list entry
    QVariant id(figureID);
    item->setData(Qt::UserRole, id);
    item->setText(f.description());
    //Add the entry to the list widget
    ui->figureListWidget->addItem(item);
}

void MainWindow::onMousePressEvent(QPoint p)
{
    QString s;
    s.append("Mouse Pressed at ");
    s.append(QString::number(p.x())).append(",");
    s.append(QString::number(p.y()));
    QMessageLogger().info(s.toUtf8().data());
}
void MainWindow::onMouseReleaseEvent(QPoint p)
{
    QString s;
    s.append("Mouse Released at ");
    s.append(QString::number(p.x()));
    s.append(",");
    s.append(QString::number(p.y()));
    QMessageLogger().info(s.toUtf8().data());

    savedChanges = false;
}

void MainWindow::on_linePushButton_clicked()
{
    ui->linePushButton->setChecked(true);
    ui->ellipsePushButton->setChecked(false);
    ui->circlePushButton->setChecked(false);
    ui->rectanglePushButton->setChecked(false);
    ui->polygonPushButton->setChecked(false);
    renderArea->setShape(RenderArea::Line);
}

void MainWindow::on_circlePushButton_clicked()
{
    ui->circlePushButton->setChecked(true);
    ui->linePushButton->setChecked(false);
    ui->ellipsePushButton->setChecked(false);
    ui->rectanglePushButton->setChecked(false);
    ui->polygonPushButton->setChecked(false);

    renderArea->setShape(RenderArea::Circle);
}

void MainWindow::on_rectanglePushButton_clicked()
{
    ui->rectanglePushButton->setChecked(true);
    ui->linePushButton->setChecked(false);
    ui->circlePushButton->setChecked(false);
    ui->ellipsePushButton->setChecked(false);
    ui->polygonPushButton->setChecked(false);

    renderArea->setShape(RenderArea::Rect);
}

void MainWindow::on_polygonPushButton_clicked()
{
    ui->rectanglePushButton->setChecked(false);
    ui->linePushButton->setChecked(false);
    ui->circlePushButton->setChecked(false);
    ui->ellipsePushButton->setChecked(false);
    ui->polygonPushButton->setChecked(true);

    renderArea->setShape(RenderArea::Polygon);
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    renderArea->setPenSize(arg1);
}

void MainWindow::on_brushColorPushButton_clicked()
{
    QColor color = QColorDialog::getColor(renderArea->getPenColor());
    renderArea->setPenColor(color);

    QString css = "background-color: rgb(";
    css.append(QString::number(color.red())).append(",");
    css.append(QString::number(color.green())).append(",");
    css.append(QString::number(color.blue())).append(");");
    ui->brushColorPushButton->setStyleSheet(css);
}

void MainWindow::on_ellipsePushButton_clicked()
{
    ui->ellipsePushButton->setChecked(true);
    ui->linePushButton->setChecked(false);
    ui->circlePushButton->setChecked(false);
    ui->rectanglePushButton->setChecked(false);
    ui->polygonPushButton->setChecked(false);

    renderArea->setShape(RenderArea::Ellipse);
}

void MainWindow::on_antiAliasCheckBox_clicked(bool checked)
{
    renderArea->setAntialiased(checked);
}

void MainWindow::on_deselectFiguresPushButton_clicked()
{
    renderArea->undoHighlight();
    ui->figureListWidget->clearSelection();
    ui->deselectFiguresPushButton->setEnabled(false);
}

void MainWindow::on_action_New_Image_triggered()
{
    NewImageDialog* newImDialog = new NewImageDialog(this);
    connect(newImDialog, &NewImageDialog::imageCreated, [this](const unsigned w, const unsigned h)
    {
        if(!savedChanges)
        {
            if(askForSave())
            {
                saveImage();
            }
        }
        this->renderArea->newImage();
        this->renderArea->setFixedWidth(w);
        this->renderArea->setFixedHeight(h);

        ui->brushColorPushButton->setStyleSheet("background-color: rgb(0,0,0);");
    });

    ui->figureListWidget->clear();
    newImDialog->show();
}

bool MainWindow::askForSave()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Dessine", "Save changes before?",
                                  QMessageBox::Yes | QMessageBox::No);
    if(reply == QMessageBox::Yes)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void MainWindow::on_actionOpen_Image_triggered()
{
    if(!savedChanges)
    {
        if(askForSave())
        {
            saveImage();
        }
    }
    openImage();
}

void MainWindow::on_actionSave_triggered()
{
    if(!savedChanges)
    {
        saveImage();
    }
}

void MainWindow::on_actionExit_triggered()
{
    quit();
}

void MainWindow::on_actionExport_as_PNG_triggered()
{
    exportImageAsPNG();
}

void MainWindow::on_actionSave_As_triggered()
{
    saveImage();
}

void MainWindow::saveToXML(QString& imageFileName)
{
    QXmlStreamWriter xmlWriter;
    QFile file(imageFileName);
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(0, "Error!", "Error opening file");
    }
    else
    {
        xmlWriter.setDevice(&file);
        renderArea->serialize(xmlWriter);
        savedChanges = true;
    }
}

void MainWindow::saveToXML(QByteArray& byteArray)
{
    QXmlStreamWriter xmlWriter(&byteArray);
    renderArea->serialize(xmlWriter);
    savedChanges = true;
}

void MainWindow::exportImageAsPNG()
{
    QString imageFileName = QFileDialog::getSaveFileName(this, tr("Save"),
                                                         QDir::homePath(),
                                                         tr("PNG Image (*.png)"));
    if(imageFileName.isNull())
        return;

    QString extension = QFileInfo(imageFileName).suffix();
    if(extension.isEmpty() || extension.isNull())
    {
        imageFileName.append(".png");
    }

    renderArea->toImage().save(imageFileName);
    QMessageLogger().info(QString("[INFO] Image saved to ").append(imageFileName).toUtf8().data());
}

void MainWindow::saveImage()
{
    if(imageFileName.isNull())
    {
        imageFileName = QFileDialog::getSaveFileName(this, tr("Save"),
                                                     QDir::homePath(),
                                                     tr("Vectorial Image (*.xml)"));
        if(imageFileName.isNull())
            return;

        QString extension = QFileInfo(imageFileName).suffix();
        if(extension.isEmpty() || extension.isNull())
        {
            imageFileName.append(".xml");
        }
    }
    saveToXML(imageFileName);
    savedChanges = true;
}

void MainWindow::openImage()
{
    imageFileName = QFileDialog::getOpenFileName(this, tr("Open"),
                                                 QDir::homePath(),
                                                 tr("Vectorial Image (*.xml)"));

    if(imageFileName.isNull())
        return;

    QMessageLogger().info(QString("opening file: ").append(imageFileName).toUtf8().data());
    QFile file(imageFileName);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageLogger().critical(QString("[CRITICAL] Cannot open file: ").append(imageFileName).toUtf8().data());
    }

    ui->figureListWidget->clear();
    QXmlStreamReader xmlData;
    xmlData.setDevice(&file);

    QSharedPointer<RenderArea> renderArea(new RenderArea);
    setupRenderArea(renderArea);
    renderArea->deserialize(xmlData);
    file.close();

    if(xmlData.hasError())
    {
        QMessageLogger().critical(QString("[CRITICAL] Failed to parse file: ").append(imageFileName).toUtf8().data());
    }
    else if(file.error() != QFile::NoError)
    {
        QMessageLogger().critical(QString("[CRITICAL] Cannot read file: ").append(imageFileName).toUtf8().data());
    }
    savedChanges = true;
}

void MainWindow::on_actionSend_to_Server_triggered()
{
    SendToServerDialog* dg = new SendToServerDialog(this);
    QByteArray data;
    saveToXML(data);
    dessine_module::HostInfo info = ConnectionUtils::getHostInfo(useLocalhostAddress);
    dg->setData(data, renderArea->width(), renderArea->height(), info);

    connect(dg, &SendToServerDialog::imagePushed, [this](const QByteArray & imageData, const QString & ticket, const dessine_module::HostInfo serverInfo)
    {
        bool alreadyRegistered = false;
        for(const auto& info : registeredServer)
        {
            if(QString(info.ipAddress) == QString(serverInfo.ipAddress) && QString(serverInfo.port) == QString(info.port))
            {
                alreadyRegistered = true;
                break;
            }
        }
        if(!alreadyRegistered)
        {
            registeredServer.append(serverInfo);
        }

        commentsHolder.registerImage(ticket, imageData);
    });

    dg->show();
}



void MainWindow::on_actionAbout_triggered()
{
    AboutDialog* a = new AboutDialog;
    a->show();
}

void MainWindow::changeEvent(QEvent* e)
{
    if(e->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    QMainWindow::changeEvent(e);
}


void MainWindow::on_actionChange_language_triggered()
{
    ChooseLanguageDialog* dd = new ChooseLanguageDialog(this);
    dd->setLanguage(applicationLanguage);

    connect(dd, &ChooseLanguageDialog::onLanguageChoosed, [this](const QString language)
    {
        changeLanguage(language);
    });

    dd->show();
}

void MainWindow::changeLanguage(const QString& language)
{
    if(language == "EN")
    {
        if(translator != nullptr)
        {
            qApp->removeTranslator(translator);
        }
        if(translator->load("simplepaint_en", ":/"))
        {
            QMessageLogger().info("Switching to en");
            qApp->installTranslator(translator);
            applicationLanguage = language;
        }
    }
    else if(language == "FR")
    {
        if(translator != nullptr)
        {
            qApp->removeTranslator(translator);
        }
        if(translator->load("simplepaint_fr", ":/"))
        {
            QMessageLogger().info("Switching to fr");
            qApp->installTranslator(translator);
            applicationLanguage = language;
        }
    }
}

void MainWindow::on_actionLocal_Server_Configuration_triggered()
{
    LocalServerConfigDialog* dg = new LocalServerConfigDialog(this);
    dg->setPort(QString(hostInfo.port));
    dg->setUseLocalhost(useLocalhostAddress);

    connect(dg, &LocalServerConfigDialog::configured, [this](const QString & port, const bool useLocalhost)
    {
        this->useLocalhostAddress = useLocalhost;
        hostInfo = ConnectionUtils::getHostInfo(useLocalhostAddress, port);
        startBackgroundTCPServer(QString(hostInfo.ipAddress), QString(hostInfo.port));
    });

    dg->show();
}

QString MainWindow::chooseServer()
{
    bool isOkPressed;
    QStringList items;
    for(const auto& server : registeredServer)
    {
        items << QString(server.hostName).append(QString(server.ipAddress)).append(":").append(QString(server.port));
    }

    QString server = QInputDialog::getItem(this, "Dessine", "Choose a server", items, 0, false, &isOkPressed);

    if(isOkPressed)
    {
        return server;
    }
    else
    {
        return "";
    }
}

void MainWindow::on_actionComments_triggered()
{
    if(registeredServer.size() == 0)
    {
        QMessageLogger().info("Must send to a server first.");
        return;
    }

    QString server;
    QString serverAddress;
    QString serverPort;
    if(registeredServer.size() == 1)
    {
        serverAddress = QString(registeredServer.at(0).ipAddress);
        serverPort = QString(registeredServer.at(0).port);
    }
    else
    {
        server = chooseServer();
        if(server.isEmpty())
        {
            return;
        }
        serverAddress = server.split(":").at(0);
        serverPort = server.split(":").at(1);
    }
    //------------------------
    dessine_module::Communication_ptr serverInstance = ConnectionUtils::getConnection(serverAddress, serverPort);


    QMessageLogger().info("Connection ok");
    for(const auto& ticket : avaibleTickets)
    {
        QMessageLogger().info(QString("Getting comments for ticket").append(ticket).toUtf8().data());
        QStringList comments = ConnectionUtils::getComments(serverInstance, ticket);
        this->commentsHolder.registerComments(ticket, comments);
    }


    avaibleTickets.clear();
    //------------------------









    QMessageLogger().info(QString("Connecting to ").append(serverAddress).append(":").append(serverPort).toUtf8().data());

    CommentsDialog* cd = new CommentsDialog(this);
    cd->setCommentsHolder(&commentsHolder);
    cd->getComments(QList<QString>(avaibleTickets), serverAddress, serverPort);
    avaibleTickets.clear();
    cd->show();
}
