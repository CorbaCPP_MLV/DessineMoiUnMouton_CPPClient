#include "commentsholder.h"

CommentsHolder::CommentsHolder()
{ }

void CommentsHolder::registerImage(const QString& ticket, const QByteArray& image)
{
    if(map.contains(ticket))
    {
        QMessageLogger().info(QString("[INFO] Registering image for ticket ").append(ticket).toUtf8().data());
        map[ticket].imageData = image;
    }
    else
    {
        commentsValue a;
        a.imageData = image;
        map.insert(ticket, a);
    }
}

void CommentsHolder::registerComments(const QString& ticket, const QStringList& comments)
{
    if(map.contains(ticket))
    {
        for(const auto& comment : comments)
        {
            if(!map[ticket].comments.contains(comment))
            {
                QMessageLogger().info(QString("[INFO] Registering comment \"").append(comment).append("\" to ticket ").append(ticket).toUtf8().data());
                map[ticket].comments.append(comment);
            }
        }
    }
    else
    {
        commentsValue a;
        a.comments = comments;
        map.insert(ticket, a);
    }
}

QByteArray CommentsHolder::getImage(const QString& ticket)
{
    if(!map.contains(ticket))
    {
        return QByteArray();
    }

    return map[ticket].imageData;
}

QStringList CommentsHolder::getComments(const QString& ticket)
{
    if(map.contains(ticket))
        return map[ticket].comments;
    return QStringList();
}


QStringList CommentsHolder::getTickets()
{
    return map.keys();
}
