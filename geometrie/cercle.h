#ifndef CERCLE_H
#define CERCLE_H
#include "figure.h"
#include <QPainter>
#include <QSharedPointer>

namespace geometry
{
using namespace std;

class Cercle: public Figure
{
private:
    Cercle(const QPoint& rectTopLeft = QPoint(0, 0), unsigned rayon = 0);

public:
    virtual ~Cercle();
    virtual QSharedPointer<Figure> copy() const;
    virtual void deplacer(const QPoint & trans);
    virtual void dessiner(QPainter& painter, bool hidden = false) const;
    virtual double surface() const;
    virtual double perimeter() const;
    virtual double distanceFromOrigin() const;
    virtual void afficher(ostream & os = cout) const;
    virtual QString description() const;
    virtual bool operator== (const Figure& f) const;
    bool operator== (const Cercle& f) const;
    virtual void serialize(QXmlStreamWriter& writer) const;


    /*!
     * \brief createCercle Static factory method for creating Cercle instances
     * \param tl Top-Left point
     * \param radius the radius of this circle
     * \param pen The pen used for this circle
     * \param brush The brush used for this circle
     * \return a pointer to the circle created
     */
    static QSharedPointer<geometry::Cercle> createCercle(const QPoint& topLeft, int radius, const QPen& pen, const QBrush& brush)
    {
        QSharedPointer<geometry::Cercle> c(new geometry::Cercle(topLeft, radius));
        c->setPen(pen);
        c->setBrush(brush);
        return c;
    }

    /*!
     * \brief createCercle Create a circle by reading the information in the given stream reader. This is used when de-serializing from XML files.
     * \param reader an xml stream reader positioned to a <cercle> tag
     * \return
     */
    static QSharedPointer<geometry::Cercle> createCercle(QXmlStreamReader& reader)
    {
        QPoint topLeft;
        QPen pen;
        QBrush brush;
        unsigned radius;
        foreach(const QXmlStreamAttribute &attr, reader.attributes())
        {
            if(attr.name().toString() == QLatin1String("topLeftX"))
            {
                topLeft.setX(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("topLeftY"))
            {
                topLeft.setY(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("radius"))
            {
                radius = attr.value().toUInt();
            }

            if(attr.name().toString() == QLatin1String("penWidth"))
            {
                pen.setWidth(attr.value().toUInt());
            }
        }

        pen.setColor(SerializationUtils::parsePenColor(reader));
        return createCercle(topLeft, radius, pen, brush);
    }

private:
    QRect rect;
    int radius;
};
}
#endif
