#ifndef ELLIPSE_H
#define ELLIPSE_H

#include "figure.h"
#include <QPainter>
#include <qmath.h>

namespace geometry
{

using namespace std;

class Ellipse: public Figure
{
private:
    Ellipse(const QRect& rect);
    Ellipse(const QPoint& topLeftRect, const QPoint& bottomRightRect);
public:
    virtual ~Ellipse();
    virtual QSharedPointer<Figure> copy() const;
    virtual void deplacer(const QPoint & trans);
    virtual void dessiner(QPainter& painter, bool hidden = false) const;
    virtual double surface() const;
    virtual double perimeter() const;
    virtual double distanceFromOrigin() const;
    virtual void afficher(ostream & os = cout) const;
    virtual QString description() const;
    bool operator== (const Ellipse& f) const;
    virtual bool operator== (const Figure& f) const;
    virtual void serialize(QXmlStreamWriter& writer) const;

    const QPoint& getCenter();

    /**
     * \brief createEllipse Static factory method for creating Ellipse instances
     * \param tl Top-Left point
     * \param br Bottom-Right point
     * \param pen The pen used for this ellipse
     * \param brush The brush used for this ellipse
     * \return a pointer to the ellipse created
     */
    static QSharedPointer<geometry::Ellipse> createEllipse(const QPoint& tl, const QPoint& br, const QPen& pen, const QBrush& brush)
    {
        QSharedPointer<geometry::Ellipse> ell(new geometry::Ellipse(tl, br));
        ell->setPen(pen);
        ell->setBrush(brush);
        return ell;
    }

    /*!
     * \brief createEllipse Create an ellipse by reading the information in the given stream reader. This is used when de-serializing from XML files.
     * \param reader an xml stream reader positioned to a <ellipse> tag
     * \return
     */
    static QSharedPointer<geometry::Ellipse> createEllipse(QXmlStreamReader& reader)
    {
        QPoint topLeft;
        QPoint bottomRight;
        QPen pen;
        QBrush brush;
        foreach(const QXmlStreamAttribute &attr, reader.attributes())
        {
            if(attr.name().toString() == QLatin1String("topLeftX"))
            {
                topLeft.setX(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("topLeftY"))
            {
                topLeft.setY(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("bottomRightX"))
            {
                bottomRight.setX(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("bottomRightY"))
            {
                bottomRight.setY(attr.value().toUInt());
            }
            if(attr.name().toString() == QLatin1String("penWidth"))
            {
                pen.setWidth(attr.value().toUInt());
            }
        }

        pen.setColor(SerializationUtils::parsePenColor(reader));
        return createEllipse(topLeft, bottomRight, pen, brush);
    }
private:

    unsigned minorAxisLenght;
    unsigned majorAxisLenght;
    QRect rect;
};
}
#endif // ELLIPSE_H
