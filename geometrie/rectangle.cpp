#include "rectangle.h"
namespace geometry
{
Rectangle::Rectangle(const QPoint & a, const QPoint & b)
    : _topLeft(a), _bottomRight(b) { }

Rectangle::Rectangle(const Rectangle & r)
    : _topLeft(r.topLeft()), _bottomRight(r.bottomRight()) { }

// deplacement-translation de valeur le point trans
void Rectangle::deplacer(const QPoint & p)
{
    _topLeft += p;
    _bottomRight += p;
}


Rectangle::~Rectangle()
{ }

QSharedPointer<Figure> Rectangle::copy() const
{
    return Rectangle::createRectangle(_topLeft, _bottomRight, pen, brush);
}


// dessin d' une Rectangle de son origine a son extremite
void Rectangle::dessiner(QPainter& painter, bool hidden) const
{
    if(!hidden)
    {
        painter.setPen(pen);
    }
    else
    {
        QPen hidden(pen);
        hidden.setColor(QColor(200,200,200));
        painter.setPen(hidden);
    }
    painter.setBrush(brush);
    painter.drawRect(QRect(_topLeft, _bottomRight));
}

double Rectangle::surface() const
{
    double width = std::abs(_bottomRight.x()-_topLeft.x());
    double height = std::abs(_bottomRight.x()-_topLeft.x());
    return width*height;
}

double Rectangle::perimeter() const
{
    double width = std::abs(_bottomRight.x()-_topLeft.x());
    double height = std::abs(_bottomRight.x()-_topLeft.x());
    return 2*width+2*height;
}

double Rectangle::distanceFromOrigin() const
{
    QRect rect(_topLeft,_bottomRight);
    return std::sqrt((rect.center().x() * rect.center().x()) + (rect.center().y() * rect.center().y()));
}



//virtual void afficher(ostream & os = cout) const
void Rectangle::afficher(std::ostream& os) const
{
    //os << "Rectangle: O" << _origine.x() << " --> E" << _extremite << endl;
}

QString Rectangle::description() const
{
    QString a("(");
    a.append(QString::number(_topLeft.x())).append(",").append(QString::number(_topLeft.y())).append(")");

    QString b("(");
    b.append(QString::number(_bottomRight.x())).append(",").append(QString::number(_bottomRight.y())).append(")");

    return QString("Rectangle from (TL) ").append(a).append(" to (BR) ").append(b);
}

QPoint Rectangle::topLeft() const
{
    return  this->_topLeft;
}

QPoint Rectangle::bottomRight() const
{
    return  this->_bottomRight;
}

bool Rectangle::operator== (const Figure& f) const
{
    assert(typeid(f) == typeid(Rectangle));
    const Rectangle& c = static_cast<const Rectangle&>(f);
    return operator ==(c);
}

bool Rectangle::operator== (const Rectangle& f) const
{
    return _topLeft == f._topLeft && _bottomRight == f._bottomRight;
}

void Rectangle::serialize(QXmlStreamWriter& writer) const
{
    writer.writeStartElement("rectangle");
    writer.writeAttribute("topLeftX", QString::number(_topLeft.x()));
    writer.writeAttribute("topLeftY", QString::number(_topLeft.y()));
    writer.writeAttribute("bottomRightX", QString::number(_bottomRight.x()));
    writer.writeAttribute("bottomRightY", QString::number(_bottomRight.y()));

    writer.writeAttribute("penWidth", QString::number(pen.width()));
    writer.writeStartElement("penColor");
    writer.writeAttribute("R", QString::number(pen.color().red()));
    writer.writeAttribute("G", QString::number(pen.color().green()));
    writer.writeAttribute("B", QString::number(pen.color().blue()));
    writer.writeEndElement();
    writer.writeEndElement();
}
}
