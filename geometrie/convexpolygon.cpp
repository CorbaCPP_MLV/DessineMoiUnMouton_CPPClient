#include "convexpolygon.h"

namespace geometry
{

ConvexPolygon::ConvexPolygon(const QVector<QPoint> &points)
{
    this->points = QVector<QPoint>(points);
}

ConvexPolygon::~ConvexPolygon()
{ }

QSharedPointer<Figure> ConvexPolygon::copy() const
{
    return ConvexPolygon::createConvexPolygon(points, pen, brush);
}

// deplacement-translation de valeur le point trans
void ConvexPolygon::deplacer(const QPoint & p)
{
    for(auto& point : points)
    {
        point += p;
    }
}

void ConvexPolygon::dessiner(QPainter& painter, bool hidden) const
{
    if(!hidden)
    {
        painter.setPen(pen);
    }
    else
    {
        QPen hidden(pen);
        hidden.setColor(QColor(200, 200, 200));
        painter.setPen(hidden);
    }
    painter.setBrush(brush);
    painter.drawConvexPolygon(QPolygon(points));
}

double ConvexPolygon::surface() const
{
    double area = 0;
    int j = points.size() - 2;
    for(int i = 0; i < points.size(); i++)
    {
        j = (i + 1) % points.size();
        area += points[i].x() * points[j].y();
        area -= points[i].y() * points[j].x();
    }

    area /= 2;
    return(area < 0 ? -area : area);
}

double ConvexPolygon::perimeter() const
{
    double perimeter = 0;
    for(int i = 0; i < points.size() - 1; i++)
    {
        QPoint p1 = points[i];
        QPoint p2 = points[i + 1];
        perimeter += std::sqrt((p2.x() - p1.x()) * (p2.x() - p1.x()) + (p2.y() - p1.y()) * (p2.y() - p1.y()));
    }

    return perimeter;
}

QPoint ConvexPolygon::getCentroid() const
{
    QPoint centroid;
    double signedArea = 0.0;
    double x0 = 0.0;
    double y0 = 0.0;
    double x1 = 0.0;
    double y1 = 0.0;
    double a = 0.0;

    // For all vertices
    int i = 0;
    for(i = 0; i < points.size(); ++i)
    {
        x0 = points[i].x();
        y0 = points[i].y();
        x1 = points[(i + 1) % points.size()].x();
        y1 = points[(i + 1) % points.size()].y();
        a = x0 * y1 - x1 * y0;
        signedArea += a;
        centroid.setX(centroid.x() + (x0 + x1)*a);
        centroid.setY(centroid.y() + (y0 + y1)*a);
    }

    signedArea *= 0.5;
    centroid.setX(centroid.x() / (6.0 * signedArea));
    centroid.setY(centroid.y() / (6.0 * signedArea));

    return centroid;
}

double ConvexPolygon::distanceFromOrigin() const
{
    QPoint centroid = getCentroid();
    return std::sqrt((centroid.x() * centroid.x()) + (centroid.y() * centroid.y()));
}

void ConvexPolygon::afficher(ostream & os) const
{
    QPoint centroid = getCentroid();
    os << "Polygon with centroid in (" << centroid.x()  << "," << centroid.y() << ")" << endl;
}

QString ConvexPolygon::description() const
{
    QPoint centroid = getCentroid();
    return QString("Polygon with centroid in (").append(QString::number(centroid.x())).append(",").append(QString::number(centroid.y())).append(")");
}

bool ConvexPolygon::operator== (const ConvexPolygon& f) const
{
    for(const auto& point : points)
    {
        if(!f.points.contains(point))
        {
            return false;
        }
    }
    return true;
}

bool ConvexPolygon::operator== (const Figure& f) const
{
    assert(typeid(f) == typeid(ConvexPolygon));
    const ConvexPolygon& c = static_cast<const ConvexPolygon&>(f);
    return operator ==(c);
}

void ConvexPolygon::serialize(QXmlStreamWriter& writer) const
{

    writer.writeStartElement("ConvexPolygon");


    for(const auto& point: points)
    {
        writer.writeStartElement("point");
        writer.writeAttribute("X", QString::number(point.x()));
        writer.writeAttribute("Y", QString::number(point.y()));
        writer.writeEndElement();
    }


    writer.writeStartElement("penWidth");
    writer.writeAttribute("value", QString::number(pen.width()));
    writer.writeEndElement();

    writer.writeStartElement("penColor");
    writer.writeAttribute("R", QString::number(pen.color().red()));
    writer.writeAttribute("G", QString::number(pen.color().green()));
    writer.writeAttribute("B", QString::number(pen.color().blue()));
    writer.writeEndElement();

    writer.writeEndElement();
}
}
