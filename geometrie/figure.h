#ifndef FIGUREH
#define FIGUREH
#include <cassert>
#include <typeinfo>
#include <iostream>
#include <QPainter>
#include <QPoint>
#include <QPen>
#include <QBrush>
#include <QString>
#include <QXmlStreamWriter>

#include "serializationutils.h"
using namespace std;

namespace geometry
{
/*!
 * \brief The Figure class is the base abstract class for all the figures
 */
class Figure
{
public:
    virtual QSharedPointer<Figure> copy() const = 0;

    virtual ~Figure() { };

    virtual void deplacer(const QPoint & trans) = 0;
    virtual void dessiner(QPainter& painter, bool hidden = false) const = 0;
    virtual void afficher(ostream & os) const = 0;
    virtual QString description() const = 0;
    virtual double surface() const = 0;
    virtual double perimeter() const = 0;
    virtual double distanceFromOrigin() const = 0;
    friend ostream & operator<< (ostream & os, const Figure & figure);
    virtual bool operator== (const Figure& f) const = 0;
    virtual void serialize(QXmlStreamWriter& writer) const = 0;


    void setPen(const QPen& pen)
    {
        this->pen = pen;
    }

    void setBrush(const QBrush& brush)
    {
        this->brush = brush;
    }

    const QPen& getPen()
    {
        return pen;
    }

    const QBrush& getBrush()
    {
        return brush;
    }

protected:
    QPen pen;
    QBrush brush;
};


}


#endif
