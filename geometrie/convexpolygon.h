#ifndef CONVEXPOLYGON_H
#define CONVEXPOLYGON_H

#include "figure.h"
#include <QPainter>
#include <qmath.h>
#include <QPolygon>

namespace geometry
{

using namespace std;

class ConvexPolygon: public Figure
{
private:
    ConvexPolygon(const QVector<QPoint> &points);
public:
    virtual ~ConvexPolygon();
    virtual QSharedPointer<Figure> copy() const;
    virtual void deplacer(const QPoint & trans);
    virtual void dessiner(QPainter& painter, bool hidden = false) const;
    virtual double surface() const;
    virtual double perimeter() const;
    virtual double distanceFromOrigin() const;
    virtual void afficher(ostream & os = cout) const;
    virtual QString description() const;
    bool operator== (const ConvexPolygon& f) const;
    virtual bool operator== (const Figure& f) const;
    virtual void serialize(QXmlStreamWriter& writer) const;


    QPoint getCentroid() const;

    /**
     * \brief createConvexPolygon Static factory method for creating ConvexPolygon instances
     * \param points the points
     * \param pen The pen used for this ConvexPolygon
     * \param brush The brush used for this ConvexPolygon
     * \return a pointer to the ConvexPolygon created
     */
    static QSharedPointer<geometry::ConvexPolygon> createConvexPolygon(const QVector<QPoint> points, const QPen& pen, const QBrush& brush)
    {
        QSharedPointer<geometry::ConvexPolygon> cp(new geometry::ConvexPolygon(points));
        cp->setPen(pen);
        cp->setBrush(brush);
        return cp;
    }

    /*!
     * \brief createConvexPolygon Create an ConvexPolygon by reading the information in the given stream reader. This is used when de-serializing from XML files.
     * \param reader an xml stream reader positioned to a <ConvexPolygon> tag
     * \return
     */
    static QSharedPointer<geometry::ConvexPolygon> createConvexPolygon(QXmlStreamReader& reader)
    {
        QVector<QPoint> points;
        QPen pen;
        QBrush brush;

        while(reader.readNextStartElement())
        {
            if(reader.name() == QLatin1String("point"))
            {
                QPoint newPoint;

                foreach(const QXmlStreamAttribute &attr, reader.attributes())
                {
                    if(attr.name().toString() == QLatin1String("X"))
                    {
                        newPoint.setX(attr.value().toUInt());
                    }
                    if(attr.name().toString() == QLatin1String("Y"))
                    {
                        newPoint.setY(attr.value().toUInt());
                    }
                }
                points.append(newPoint);
                reader.skipCurrentElement();
            }

            if(reader.name() == QLatin1String("penWidth"))
            {
                int penWidth = reader.attributes().at(0).value().toUInt();
                pen.setWidth(penWidth);
                reader.skipCurrentElement();
            }

            if(reader.name() == QLatin1String("penColor"))
            {
                int r, g, b;
                foreach(const QXmlStreamAttribute &attr, reader.attributes())
                {
                    if(attr.name().toString() == QLatin1String("R"))
                    {
                        r = attr.value().toUInt();
                    }

                    if(attr.name().toString() == QLatin1String("G"))
                    {
                        g = attr.value().toUInt();
                    }

                    if(attr.name().toString() == QLatin1String("B"))
                    {
                        b = attr.value().toUInt();
                    }
                }
                pen.setColor(QColor(r, g, b));
                reader.skipCurrentElement();
            }
        }

        return createConvexPolygon(points, pen, brush);
    }
private:

    QVector<QPoint> points;
};
}
#endif // CONVEXPOLYGON_H
