#include "ligne.h"
namespace geometry
{
Ligne::Ligne(const QPoint & a, const QPoint & b)
    : origin(a), extremity(b) { }

// deplacement-translation de valeur le point trans
void Ligne::deplacer(const QPoint & p)
{
    origin += p;
    extremity += p;
}


Ligne::~Ligne()
{ }

QSharedPointer<Figure> Ligne::copy() const
{
    return Ligne::createLine(origin, extremity, pen, brush);
}


// dessin d' une ligne de son origine a son extremite
void Ligne::dessiner(QPainter& painter, bool hidden) const
{
    if(!hidden)
    {
        painter.setPen(pen);
    }
    else
    {
        QPen hidden(pen);
        hidden.setColor(QColor(200, 200, 200));
        painter.setPen(hidden);
    }
    painter.setBrush(brush);
    painter.drawLine(origin, extremity);
}

double Ligne::surface() const
{
    return 0.0;
}

double Ligne::perimeter() const
{
    return 0;
}

double Ligne::distanceFromOrigin() const
{
    //https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    double d = std::abs(extremity.x() * origin.y() - origin.x() * extremity.y()) /
             std::sqrt(((extremity.y() - origin.y()) * (extremity.y() - origin.y())) +
                      ((extremity.x() - origin.x()) * (extremity.x() - origin.x())));
    return d;
}

//virtual void afficher(ostream & os = cout) const
void Ligne::afficher(std::ostream& os) const
{
    //os << "Ligne: O" << _origine.x() << " --> E" << _extremite << endl;
}

QPoint Ligne::getOrigin() const
{
    return  this->origin;
}

QPoint Ligne::getExtremity() const
{
    return  this->extremity;
}

QString Ligne::description() const
{
    QString a("(");
    a.append(QString::number(origin.x())).append(",").append(QString::number(origin.y())).append(")");

    QString b("(");
    b.append(QString::number(extremity.x())).append(",").append(QString::number(extremity.y())).append(")");

    return QString("Ligne from ").append(a).append(" to ").append(b);
}

bool Ligne::operator== (const Figure& f) const
{
    assert(typeid(f) == typeid(Ligne));
    const Ligne& c = static_cast<const Ligne&>(f);
    return operator ==(c);
}

bool Ligne::operator== (const Ligne& f) const
{
    return origin == f.getOrigin() && extremity == f.getExtremity();
}

void Ligne::serialize(QXmlStreamWriter& writer) const
{
    writer.writeStartElement("ligne");
    writer.writeAttribute("origineX", QString::number(origin.x()));
    writer.writeAttribute("origineY", QString::number(origin.y()));
    writer.writeAttribute("extremiteX", QString::number(extremity.x()));
    writer.writeAttribute("extremiteY", QString::number(extremity.y()));

    writer.writeAttribute("penWidth", QString::number(pen.width()));
    writer.writeStartElement("penColor");
    writer.writeAttribute("R", QString::number(pen.color().red()));
    writer.writeAttribute("G", QString::number(pen.color().green()));
    writer.writeAttribute("B", QString::number(pen.color().blue()));
    writer.writeEndElement();
    writer.writeEndElement();
}
}
