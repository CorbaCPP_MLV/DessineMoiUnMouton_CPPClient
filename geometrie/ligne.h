#ifndef LIGNE_H
#define LIGNE_H
#include "figure.h"
#include <QPainter>
#include <QPoint>
#include <QPointer>    
#include <cmath>

namespace geometry
{

using namespace std;

class Ligne: public Figure
{

private:
    Ligne(const QPoint & a , const QPoint & b);
public:
    virtual ~Ligne();

    virtual QSharedPointer<Figure> copy() const;
    virtual void deplacer(const QPoint & trans);
    virtual void dessiner(QPainter& painter, bool hidden = false) const;
    virtual double surface() const;
    virtual double perimeter() const;
    virtual double distanceFromOrigin() const;
    virtual void afficher(ostream & os = cout) const;
    virtual QString description() const;
    virtual bool operator== (const Figure& f) const;
    bool operator== (const Ligne& f) const;
    virtual void serialize(QXmlStreamWriter& writer) const;


    QPoint getOrigin() const;
    QPoint getExtremity() const;

    /*!
     * \brief createLigne Static factory method for creating Ligne instances
     * \param a Top-Left point
     * \param b Bottom-Right point
     * \param pen The pen used for this line
     * \param brush The brush used for this line
     * \return a pointer to the line created
     */
    static QSharedPointer<Ligne> createLine(const QPoint& a, const QPoint& b, const QPen& pen, const QBrush& brush)
    {
        QSharedPointer<Ligne> ll(new Ligne(a, b));
        ll->setPen(pen);
        ll->setBrush(brush);
        return ll;
    }

    /*!
     * \brief createLigne Create a line by reading the information in the given stream reader. This is used when de-serializing from XML files.
     * \param reader an xml stream reader positioned to a <ligne> tag
     * \return
     */
    static QSharedPointer<geometry::Ligne> createLine(QXmlStreamReader& reader)
    {
        QPoint a;
        QPoint b;
        QPen pen;
        QBrush brush;
        foreach(const QXmlStreamAttribute &attr, reader.attributes())
        {
            if(attr.name().toString() == QLatin1String("origineX"))
            {
                a.setX(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("origineY"))
            {
                a.setY(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("extremiteX"))
            {
                b.setX(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("extremiteY"))
            {
                b.setY(attr.value().toUInt());
            }
            if(attr.name().toString() == QLatin1String("penWidth"))
            {
                pen.setWidth(attr.value().toUInt());
            }
        }

        pen.setColor(SerializationUtils::parsePenColor(reader));
        return createLine(a, b, pen, brush);
    }

private:
    QPoint origin;
    QPoint extremity;
};
}
#endif
