#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "figure.h"
#include <QPainter>
#include <QPoint>
#include <cmath>
namespace geometry
{

using namespace std;

class Rectangle: public Figure
{

private:
    Rectangle(const QPoint & topLeft , const QPoint & bottomRight);
    Rectangle(const Rectangle & r);
public:
    virtual ~Rectangle();

    virtual QSharedPointer<Figure> copy() const;
    virtual void deplacer(const QPoint & trans);
    virtual void dessiner(QPainter& painter, bool hidden = false) const;
    virtual double surface() const;
    virtual double perimeter() const;
    virtual double distanceFromOrigin() const;
    virtual void afficher(ostream & os = cout) const;
    virtual QString description() const;
    virtual bool operator== (const Figure& f) const;
    bool operator== (const Rectangle& f) const;
    virtual void serialize(QXmlStreamWriter& writer) const;

    QPoint topLeft() const;
    QPoint bottomRight() const;

    /**
     * \brief createRectangle Static factory method for creating Rectangle instances
     * \param tl Top-Left point
     * \param br Bottom-Right point
     * \param pen The pen used for this rectangle
     * \param brush The brush used for this rectangle
     * \return a pointer to the rectangle created
     */
    static QSharedPointer<Rectangle> createRectangle(const QPoint& tl, const QPoint& br, const QPen& pen, const QBrush& brush)
    {
        QSharedPointer<Rectangle> rect(new Rectangle(tl, br));
        rect->setPen(pen);
        rect->setBrush(brush);
        return rect;
    }

    /*!
     * \brief createRectangle Create a rectangle by reading the information in the given stream reader. This is used when de-serializing from XML files.
     * \param reader an xml stream reader positioned to a <rectangle> tag
     * \return
     */
    static QSharedPointer<geometry::Rectangle> createRectangle(QXmlStreamReader& reader)
    {
        QPoint topLeft;
        QPoint bottomRight;
        QPen pen;
        QBrush brush;
        foreach(const QXmlStreamAttribute &attr, reader.attributes())
        {
            if(attr.name().toString() == QLatin1String("topLeftX"))
            {
                topLeft.setX(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("topLeftY"))
            {
                topLeft.setY(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("bottomRightX"))
            {
                bottomRight.setX(attr.value().toUInt());
            }

            if(attr.name().toString() == QLatin1String("bottomRightY"))
            {
                bottomRight.setY(attr.value().toUInt());
            }
            if(attr.name().toString() == QLatin1String("penWidth"))
            {
                pen.setWidth(attr.value().toUInt());
            }
        }

        pen.setColor(SerializationUtils::parsePenColor(reader));
        return createRectangle(topLeft, bottomRight, pen, brush);
    }

private:
    QPoint _topLeft;
    QPoint _bottomRight;
};
}
#endif // RECTANGLE_H
