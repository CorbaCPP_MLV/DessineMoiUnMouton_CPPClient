#ifndef SERIALIZATIONUTILS_H
#define SERIALIZATIONUTILS_H

#include <QColor>

namespace geometry
{

class SerializationUtils
{
public:
    /*!
     * \brief parsePenColor Parse the pen color from an xml stream reader
     * \param reader
     * \return
     */
    static QColor parsePenColor(QXmlStreamReader& reader)
    {
        reader.readNextStartElement();

        //Read pen colors
        int r, g, b;
        foreach(const QXmlStreamAttribute &attr, reader.attributes())
        {
            if(attr.name().toString() == QLatin1String("R"))
            {
                r = attr.value().toUInt();
            }

            if(attr.name().toString() == QLatin1String("G"))
            {
                g = attr.value().toUInt();
            }

            if(attr.name().toString() == QLatin1String("B"))
            {
                b = attr.value().toUInt();
            }
        }
        return QColor(r, g, b);
    }
};
}
#endif // SERIALIZATIONUTILS_H
