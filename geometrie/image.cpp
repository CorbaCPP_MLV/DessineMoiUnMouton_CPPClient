#include <stdio.h>
#include <cassert>
#include <algorithm>

#include "image.h"
#include "ligne.h"
#include "cercle.h"
namespace geometry
{


Image::Image() : nextImageIdx(0)
{}

Image::Image(const Image & image) : nextImageIdx(0)
{
    /*for(unsigned i = 0; i < image.getNombre(); i++)
    {
        ajouter(image);
    }*/
}

Image::~Image()
{ }

QSharedPointer<Figure> Image::copy() const
{
    return QSharedPointer<Figure>(new Image());
}

const QSharedPointer<geometry::Figure> Image::getFigure(int index) const
{
    return figures[index];
}

const QHash<unsigned, QSharedPointer<Figure> > Image::getFigures() const
{
    return figures;
}

bool Image::contains(const QSharedPointer<Figure>& f)
{
    return figures.values().contains(f);
}

// ajout d' une figure a une image
int Image::ajouter(const QSharedPointer<Figure>& f)
{
    if(figures.values().contains(f))
    {
        QMessageLogger().warning(QString("[WARNING] ").append(f->description()).append(" already in image").toUtf8().data());
        return -1;
    }

    QMessageLogger().info(QString("[INFO] ").append(f->description()).append(" added").toUtf8().data());
    figures.insert(nextImageIdx + 1, f);
    return ++nextImageIdx;
}


void Image::removeImage(const unsigned figureID)
{
    if(figures.keys().contains(figureID))
    {
        figures.remove(figureID);
    }
}

int Image::getNombre() const
{
    return figures.size();
}

void Image::deplacer(const QPoint& p)
{
    for(const auto& figure : figures.values())
    {
        figure->deplacer(p);
    }
}

void Image::dessiner(QPainter& painter, bool hidden) const
{
    for(const auto& figureID : figures.keys())
    {
        bool hidden = !highlightedFigures.contains(figureID);
        figures.value(figureID)->dessiner(painter, hidden && highlightedFigures.size() > 0);
    }
}

double Image::surface() const
{
    double sum = 0.0;
    for(const auto& figure : figures.values())
    {
        sum += figure->surface();
    }
    return sum;
}

double Image::perimeter() const
{
    return 0;
}

double Image::distanceFromOrigin() const
{
    return 0;
}

void Image::afficher(ostream & os) const
{
    os << "Image:" << endl;
    for(unsigned i = 0; i < figures.size(); i++)
    {
        os << "figure #" <<  i << ": " << figures[i];
    }
}

QString Image::description() const
{
    return QString("Image");
}

ostream & operator<< (ostream & os, const Image & image)
{
    image.afficher(os);
    return os;
}

bool Image::operator== (const Figure& f) const
{
    assert(typeid(f) == typeid(Image));
    const Image& c = static_cast<const Image&>(f);
    return operator ==(c);
}

bool Image::operator== (const Image& f) const
{
    return false;
}

void Image::serialize(QXmlStreamWriter& writer) const
{
    writer.writeStartElement("image");
    for(const auto& f : figures.values())
    {
        f->serialize(writer);
    }
    writer.writeEndElement();
}

void Image::highlightFigure(int figureID)
{
    if(!highlightedFigures.contains(figureID))
    {
        highlightedFigures.append(figureID);
    }
    else
    {
        highlightedFigures.removeOne(figureID);
    }

}

void Image::undoHighlight()
{
    highlightedFigures.clear();
}

}
