#ifndef IMAGE_H
#define IMAGE_H
#include "figure.h"
#include <QPainter>
#include <QHash>

#include "cercle.h"
#include "ellipse.h"
#include "ligne.h"
#include "rectangle.h"
#include "convexpolygon.h"

using namespace std;
namespace geometry
{
class Image : public Figure
{

public:
    Image();

    Image(const Image& image);
    virtual QSharedPointer<Figure> copy() const;
    virtual ~Image();

    virtual void deplacer(const QPoint & trans);
    virtual void dessiner(QPainter& painter, bool hidden = false) const;
    virtual double surface() const;
    virtual double perimeter() const;
    virtual double distanceFromOrigin() const;
    virtual void afficher(ostream & os = cout) const;
    virtual QString description() const;
    virtual bool operator== (const Figure& f) const;
    bool operator== (const Image& f) const;
    virtual void serialize(QXmlStreamWriter& writer) const;


    const QSharedPointer<geometry::Figure> getFigure(int index) const;
    const QHash<unsigned, QSharedPointer<geometry::Figure>> getFigures() const;
    int getNombre() const;

    /* ajout d'une copie dans le container de figures */
    int ajouter(const QSharedPointer<Figure>& f);
    void removeImage(const unsigned figureID);
    bool contains(const QSharedPointer<Figure>& f);

    void highlightFigure(int figureID);
    void undoHighlight();

    /*!
     * \brief createImage Create an image by reading the information in the given stream reader. This is used when de-serializing from XML files.
     * \param reader an xml stream reader positioned to a <image> tag
     * \return
     */
    static QSharedPointer<geometry::Image> createImage(QXmlStreamReader& reader)
    {
        QSharedPointer<geometry::Image> image(new Image);

        QMessageLogger().info(QString("Got Image").toUtf8().data());
        if(reader.name() == "image")
        {
            //while(reader.readNextStartElement())
            while (!reader.atEnd() && !reader.hasError())
            {
                reader.readNext();

                if(!reader.isStartElement())
                    continue;
                if(reader.name() == "cercle")
                {
                    image->ajouter(Cercle::createCercle(reader));
                }
                else if(reader.name() == "ellipse")
                {
                    image->ajouter(Ellipse::createEllipse(reader));
                }
                else if(reader.name() == "ligne")
                {
                    image->ajouter(Ligne::createLine(reader));
                }
                else if(reader.name() == "rectangle")
                {
                    image->ajouter(Rectangle::createRectangle(reader));
                }
                else if(reader.name() == "ConvexPolygon")
                {
                    QMessageLogger().info("CREATING POLYGON");
                    image->ajouter(ConvexPolygon::createConvexPolygon(reader));
                }
            }
        }
        return image;
    }

private:
    /*!
     * \brief figures Container for the figures this image holds. This map take an unisigned integer as index of the figure, and a pointer to the figure.
     */
    QHash<unsigned, QSharedPointer<geometry::Figure>> figures;

    /*!
     * \brief nextImageIdx Index that is used as ID with a figure to be added
     */
    unsigned nextImageIdx;

    /*!
     * \brief highlightedFigures
     */
    QList<int> highlightedFigures;
};
}
#endif
