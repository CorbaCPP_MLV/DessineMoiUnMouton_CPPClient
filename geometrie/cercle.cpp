#include "cercle.h"
#include <cmath>

namespace geometry
{

Cercle::Cercle(const QPoint& rectTopLeft, unsigned rayon)
    : radius(rayon)
{
    rect = QRect(rectTopLeft, QPoint(rectTopLeft.x() + rayon, rectTopLeft.y() + rayon));
}

Cercle::~Cercle()
{ }

QSharedPointer<Figure> Cercle::copy() const
{
    return Cercle::createCercle(rect.topLeft(), radius, pen, brush);
}

// deplacement-translation de valeur le point trans
void Cercle::deplacer(const QPoint & p)
{
    rect.translate(p);
}

void Cercle::dessiner(QPainter& painter, bool hidden) const
{
    if(!hidden)
    {
        painter.setPen(pen);
    }
    else
    {
        QPen hidden(pen);
        hidden.setColor(QColor(200, 200, 200));
        painter.setPen(hidden);
    }
    painter.setBrush(brush);
    painter.drawEllipse(rect);
}


double Cercle::surface() const
{
    const double PI = std::atan(1.0) * 4;
    return PI * radius * radius;
}

double Cercle::perimeter() const
{
    const double PI = std::atan(1.0) * 4;
    return PI * radius * 2;
}

double Cercle::distanceFromOrigin() const
{
    return std::sqrt((rect.center().x() * rect.center().x()) + (rect.center().y() * rect.center().y()));

}

void Cercle::afficher(ostream & os) const
{
    //os << "Cercle en (" << this->_centre.x()  << "," << this->_centre.y() << ") de rayon " << this->_rayon << endl;
}

QString Cercle::description() const
{
    return QString("Cercle en (").append(QString::number(rect.center().x())).append(",").append(QString::number(rect.center().y())).append(") de rayon ").append(QString::number(radius));

}

bool Cercle::operator== (const Figure& f) const
{
    assert(typeid(f) == typeid(Cercle));
    const Cercle& c = static_cast<const Cercle&>(f);
    return operator ==(c);
}

bool Cercle::operator== (const Cercle& f) const
{
    return this->rect == f.rect;
}

void Cercle::serialize(QXmlStreamWriter& writer) const
{
    writer.writeStartElement("cercle");
    writer.writeAttribute("topLeftX", QString::number(rect.topLeft().x()));
    writer.writeAttribute("topLeftY", QString::number(rect.topLeft().y()));
    writer.writeAttribute("radius", QString::number(radius));

    writer.writeAttribute("penWidth", QString::number(pen.width()));
    writer.writeStartElement("penColor");
    writer.writeAttribute("R", QString::number(pen.color().red()));
    writer.writeAttribute("G", QString::number(pen.color().green()));
    writer.writeAttribute("B", QString::number(pen.color().blue()));
    writer.writeEndElement();

    writer.writeEndElement();
}



}
