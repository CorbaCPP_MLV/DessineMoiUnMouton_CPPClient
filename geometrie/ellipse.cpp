#include "ellipse.h"
#include <cmath>

namespace geometry
{

Ellipse::Ellipse(const QRect& rect)
{
    this->rect = QRect(rect);
    minorAxisLenght = rect.width() > rect.height() ? rect.height() : rect.width();
    majorAxisLenght = rect.width() < rect.height() ? rect.height() : rect.width();
}

Ellipse::Ellipse(const QPoint& topLeftRect, const QPoint& bottomRightRect) : Ellipse(QRect(topLeftRect, bottomRightRect))
{ }

Ellipse::~Ellipse()
{ }

QSharedPointer<Figure> Ellipse::copy() const
{
    return Ellipse::createEllipse(rect.topLeft(), rect.bottomRight(), pen, brush);
}

// deplacement-translation de valeur le point trans
void Ellipse::deplacer(const QPoint & p)
{
    rect.translate(p);
}

void Ellipse::dessiner(QPainter& painter, bool hidden) const
{
    if(!hidden)
    {
        painter.setPen(pen);
    }
    else
    {
        QPen hidden(pen);
        hidden.setColor(QColor(200, 200, 200));
        painter.setPen(hidden);
    }
    painter.setBrush(brush);
    painter.drawEllipse(rect);

}

double Ellipse::surface() const
{
    const double PI = std::atan(1.0) * 4;
    return PI * minorAxisLenght * majorAxisLenght;
}

double Ellipse::perimeter() const
{
    const double PI = std::atan(1.0) * 4;
    return 2 * PI * std::sqrt((majorAxisLenght * majorAxisLenght + minorAxisLenght * minorAxisLenght) / 2);
}

double Ellipse::distanceFromOrigin() const
{
    return std::sqrt((rect.center().x() * rect.center().x()) + (rect.center().y() * rect.center().y()));
}

const QPoint& Ellipse::getCenter()
{
    return rect.center();
}

void Ellipse::afficher(ostream & os) const
{
    //os << "Ellipse en (" << this->_centre.x()  << "," << this->_centre.y() << ") de rayon " << this->_rayon << endl;
}

QString Ellipse::description() const
{
    return QString("Ellipse en (").append(QString::number(rect.center().x())).append(",").append(QString::number(rect.center().y())).append(")");
}

bool Ellipse::operator== (const Ellipse& f) const
{
    return this->rect == f.rect;
}

bool Ellipse::operator== (const Figure& f) const
{
    assert(typeid(f) == typeid(Ellipse));
    const Ellipse& c = static_cast<const Ellipse&>(f);
    return operator ==(c);
}

void Ellipse::serialize(QXmlStreamWriter& writer) const
{
    writer.writeStartElement("ellipse");
    writer.writeAttribute("topLeftX", QString::number(rect.topLeft().x()));
    writer.writeAttribute("topLeftY", QString::number(rect.topLeft().y()));
    writer.writeAttribute("bottomRightX", QString::number(rect.bottomRight().x()));
    writer.writeAttribute("bottomRightY", QString::number(rect.bottomRight().y()));

    writer.writeAttribute("penWidth", QString::number(pen.width()));
    writer.writeStartElement("penColor");
    writer.writeAttribute("R", QString::number(pen.color().red()));
    writer.writeAttribute("G", QString::number(pen.color().green()));
    writer.writeAttribute("B", QString::number(pen.color().blue()));
    writer.writeEndElement();
    writer.writeEndElement();
}
}
