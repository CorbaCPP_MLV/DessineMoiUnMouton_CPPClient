#ifndef SENDTOSERVERDIALOG_H
#define SENDTOSERVERDIALOG_H

#include <QDialog>
#include <QMessageLogger>
#include <QFile>
#include <QSharedPointer>
#include <QTextStream>
#include <QMessageBox>
#include <QStandardItemModel>

#include "CORBA/connectionutils.h"
#include "CORBA/orb_interface.hh"
#include "CORBA/backgroundconnection.h"

namespace Ui
{
class SendToServerDialog;
}

class SendToServerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SendToServerDialog(QWidget *parent = 0);
    ~SendToServerDialog();


    void setData(QByteArray& data, unsigned width, unsigned height, dessine_module::HostInfo &info);

private slots:
    void on_sendPushButton_clicked();
    void on_closePushButton_clicked();

signals:
    void imagePushed(const QByteArray&, const QString&, const dessine_module::HostInfo);

private:
    bool connectToServer();

    dessine_module::Communication_ptr serverInstance;
    dessine_module::HostInfo info;
    QByteArray data;
    unsigned imageHeight;
    unsigned imageWidth;
    Ui::SendToServerDialog *ui;
};

#endif // SENDTOSERVERDIALOG_H
