#include "dessine_module_dessine_i.h"

dessine_module_Dessine_i::dessine_module_Dessine_i(){

}
dessine_module_Dessine_i::~dessine_module_Dessine_i(){
}

// Methods corresponding to IDL attributes and operations
::CORBA::Boolean dessine_module_Dessine_i::sendImage(const dessine_module::Image& img, const dessine_module::HostInfo& destination)
{ }

::CORBA::Boolean dessine_module_Dessine_i::sendComments(const dessine_module::StringArray& comments, const dessine_module::HostInfo& destination)
{ }

::CORBA::Boolean dessine_module_Dessine_i::sendImageWithComments(const dessine_module::Image& img, const dessine_module::StringArray& comments, const dessine_module::HostInfo& destination)
{ }
