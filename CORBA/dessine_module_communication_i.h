#ifndef DESSINE_MODULE_COMMUNICATION_I_H
#define DESSINE_MODULE_COMMUNICATION_I_H

#include <iostream>
#include <QImage>
#include <QDebug>
#include <QString>
#include <QList>
#include <vector>
#include <boost/signals2.hpp>
#include "orb_interface.hh"

using namespace boost::signals2;

class dessine_module_Communication_i : public POA_dessine_module::Communication
{
private:
    //virtual ~dessine_module_Communication_i();

public:
    // standard constructor
    dessine_module_Communication_i();
    virtual ~dessine_module_Communication_i();

    // methods corresponding to defined IDL attributes and operations
    void registerProducer(const dessine_module::HostInfo& info);
    char* pushImage(const dessine_module::Image& img, const dessine_module::HostInfo& producerInfo);
    void readyForPull(const char* ticket, const dessine_module::HostInfo& producerInfo);
    dessine_module::StringArray* pullComments(const char* ticket);
    void disconnect(const dessine_module::HostInfo& info);

    //Signals
    signal<void (const dessine_module::HostInfo&)> onRegisterProducer;
    signal<void (const dessine_module::Image&, const dessine_module::HostInfo&)> onPushImage;
    signal<void (const QString&, const dessine_module::HostInfo&)> onReadyForPull;
    signal<void (QList<QString>&)> onPullComment;
};


#endif // DESSINE_MODULE_COMMUNICATION_I_H
