#include "backgroundconnection.h"

BackgroundConnection::BackgroundConnection()
{ }

const QSharedPointer<Connection> BackgroundConnection::getConnection()
{
    return connection;
}

void BackgroundConnection::stopServer()
{
    connection->stop();
    quit();
    wait();
}

void BackgroundConnection::run()
{
    connection->start();
}
