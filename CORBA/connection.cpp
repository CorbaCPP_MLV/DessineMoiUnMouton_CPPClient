#include "connection.h"

Connection::Connection() : Connection(0,nullptr)
{ }

Connection::Connection(int argc, char** argv)
{
    this->argc = argc;
    this->argv = argv;
    init();
}

const QSharedPointer<dessine_module_Communication_i> Connection::getInstance()
{
    return instance;
}


void Connection::start()
{
    orb->run();
}

void Connection::stop()
{
    orb->destroy();
}

void Connection::onReadyForPull(const QString& ticket, const dessine_module::HostInfo& info)
{
    mutex.lock();
    receivedTicket.append(ticket);
    mutex.unlock();
    emit readyForPull(ticket, info);
}


QList<QString> Connection::tickets()
{
    QList<QString> copy;
    mutex.lock();
    copy = QList<QString>(receivedTicket);
    receivedTicket.clear();
    mutex.unlock();

    return copy;
}


const QString Connection::getIOR() const
{
    return QString(ior);
}

dessine_module::HostInfo Connection::getHostInfo()
{
    dessine_module::HostInfo info;
    info.hostName = "davide";
    info.type = dessine_module::HostType::CLIENT;
    info.ior = ior.toStdString().data();



    //info.ipAddress = "192.168.102.9";
    info.ipAddress = "127.0.0.1";
    info.port = "1234";

    return info;
}

void Connection::init()
{
    //CosNaming::NamingContext_var rootContext;

    try
    {
        // Initialise the ORB.
        orb = CORBA::ORB_init(argc, argv);

        // Obtain a reference to the root POA.
        CORBA::Object_var rootPOA = orb->resolve_initial_references("RootPOA");
        PortableServer::POA_var poa = PortableServer::POA::_narrow(rootPOA);

        // We allocate the objects on the heap.  Since these are reference
        // counted objects, they will be deleted by the POA when they are no
        // longer needed.
        instance = QSharedPointer<dessine_module_Communication_i>(new dessine_module_Communication_i());
        instance->onReadyForPull.connect(bind(&Connection::onReadyForPull, this,_1,_2));

        // Activate the objects.  This tells the POA that the objects are
        // ready to accept requests.
        PortableServer::ObjectId_var iid = poa->activate_object(instance.data());

        //Obtain a reference to each object and output the stringified
        // IOR to stdout
        {
            // IDL interface: bytes_transmission::Dessine
            CORBA::Object_var ref = instance->_this();
            CORBA::String_var sior(orb->object_to_string(ref));
            std::cout << "IDL object bytes_transmission::Dessine IOR = '" << (char*)sior << "'" << std::endl;
            this->ior = QString(sior);
        }

        // Obtain a POAManager, and tell the POA to start accepting
        // requests on its objects.
        PortableServer::POAManager_var pman = poa->the_POAManager();
        pman->activate();

        emit connectionReady();
    }
    catch(CORBA::TRANSIENT&)
    {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
                  << "server." << std::endl;
    }
    catch(CORBA::SystemException& ex)
    {
        std::cerr << "Caught a CORBA::" << ex._name() << std::endl;
    }
    catch(CORBA::Exception& ex)
    {
        std::cerr << "Caught CORBA::Exception: " << ex._name() << std::endl;
    }
}
