#ifndef CORBACONNECTION_H
#define CORBACONNECTION_H

#include <QFile>
#include <QList>
#include <QString>
#include <QTextStream>
#include <QNetworkInterface>
#include <QHostInfo>
#include "CORBA/orb_interface.hh"

using namespace CosNaming;

class ConnectionUtils
{
private:
    ConnectionUtils();
public:
    /*!
     * \brief getConnection Create a CORBA connection between this and another host using the specified address/port., and get a reference to a Communication object
     * \param host the IP address of the host to connect to
     * \param port the port address
     * \param argv
     * \return a pointer to the (remote) Communication object
     */
    static dessine_module::Communication_ptr getConnection(const QString& host = "127.0.0.1", const QString& port = "1234", char *argv[] = nullptr)
    {
        try
        {
            // Initialise the ORB.
            int argc = 0;
            CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);

            //Assemble the connection string
            QString connectionString("corbaloc::");
            connectionString.append(host);
            connectionString.append(":").append(port);
            connectionString.append("/NameService");
            CORBA::Object_var dessine = orb->string_to_object(connectionString.toStdString().data());

            //Narrow the remote context
            NamingContextExt_ptr remoteContext = CosNaming::NamingContextExt::_narrow(dessine);

            //Get all the bindings from remote
            BindingList_var bindingList = new BindingList();
            BindingList_out bindingListOut(bindingList);

            BindingIterator_var iterator;
            BindingIterator_out iteratorOut(iterator);

            remoteContext->list(5, bindingListOut, iteratorOut);

            for(unsigned i = 0; i < 5; i++)
            {
                Binding binding = bindingListOut[i];
                QString name = remoteContext->to_string(binding.binding_name);

                //If current binding has the specified name, return a reference to it
                if(name == "DESSINE_MAITRISSE")
                {
                    return dessine_module::Communication::_narrow(remoteContext->resolve_str(name.toStdString().data()));
                }
            }
        }
        catch(CORBA::TRANSIENT&)
        {
            QMessageLogger().critical("Caught system exception TRANSIENT -- unable to contact the server.");
        }
        catch(CORBA::SystemException& ex)
        {
            QMessageLogger().critical(QString("Caught a CORBA::").append(ex._name()).toUtf8().data());
        }
        catch(CORBA::Exception& ex)
        {
            QMessageLogger().critical(QString("Caught CORBA::Exception: ").append(ex._name()).toUtf8().data());
        }

        return nullptr;
    }

    /*!
     * \brief XMLImageToCORBAImage Convert a QByteArray containing the xml data of an image to a custom defined Image object (See the IDL)
     * \param xmlImageData the byte array containing the xml data
     * \param width the width of the image
     * \param height the height of the image
     * \return an Image object
     */
    static dessine_module::Image XMLImageToCORBAImage(const QByteArray& xmlImageData, unsigned width, unsigned height)
    {
        dessine_module::Image im;

        im.data = dessine_module::ByteArray();
        im.data.length(xmlImageData.size());
        memcpy(im.data.get_buffer(), xmlImageData.data(), im.data.length());

        im.bytesCount = xmlImageData.size();
        im.width = width;
        im.height = height;

        return im;
    }

    /*!
     * \brief toStringArray Convert a QList of string to a StringArray object
     * \param strings
     * \return
     */
    static dessine_module::StringArray toStringArray(const QList<QString>& strings)
    {
        dessine_module::StringArray_var sa = new dessine_module::StringArray;
        sa->length(strings.size());

        for(int i = 0; i < strings.size(); i++)
        {
            sa[i] = CORBA::string_dup(strings.at(i).toStdString().data());
        }
        return *sa._retn();
    }


    /*!
     * \brief toQStringList Convert a StringArray object to a QList
     * \param strings
     * \return
     */
    static QList<QString> toQStringList(const dessine_module::StringArray& strings)
    {
        QList<QString> str;
        for(unsigned i = 0; i < strings.length(); i++)
        {
            str.append(QString(strings[i]));
        }
        return str;
    }


    static dessine_module::HostInfo getHostInfo(const bool localHost = true, const QString& listeningPort = "1235")
    {
        QString ipAddress;
        QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();

        // use the first non-localhost IPv4 address
        for(int i = 0; i < ipAddressesList.size(); ++i)
        {
            if(localHost)
            {
                if(ipAddressesList.at(i) == QHostAddress::LocalHost && ipAddressesList.at(i).toIPv4Address())
                {
                    ipAddress = ipAddressesList.at(i).toString();
                    break;
                }
            }
            else
            {
                if(ipAddressesList.at(i) != QHostAddress::LocalHost &&
                        ipAddressesList.at(i).toIPv4Address())
                {
                    ipAddress = ipAddressesList.at(i).toString();
                    break;
                }
            }
        }

        // if we did not find one, use IPv4 localhost
        if(ipAddress.isEmpty())
        {
            ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
        }

        dessine_module::HostInfo info;
        info.hostName = CORBA::string_dup(QHostInfo::localHostName().toStdString().data());
        info.ipAddress = CORBA::string_dup(ipAddress.toStdString().data());
        info.port = CORBA::string_dup(listeningPort.toStdString().data());
        info.type = dessine_module::HostType::CLIENT;
        return info;
    }


    static QStringList getComments(const QList<QString>& tickets, const QString& address, const QString& port)
    {
        dessine_module::Communication_ptr serverInstance = ConnectionUtils::getConnection(address, port);
        QStringList comments;
        for(const auto& ticket : tickets)
        {
            dessine_module::StringArray* c = serverInstance->pullComments(ticket.toStdString().data());
            comments.append(ConnectionUtils::toQStringList(*c));
        }

        return comments;
    }

    static QStringList getComments(dessine_module::Communication_ptr serverInstance, const QString& ticket)
    {
        dessine_module::StringArray* c = serverInstance->pullComments(ticket.toStdString().data());
        return ConnectionUtils::toQStringList(*c);
    }

};

#endif // CORBACONNECTION_H
