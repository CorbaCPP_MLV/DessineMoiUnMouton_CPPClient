#ifndef CONNECTION_H
#define CONNECTION_H

#include <QThread>
#include <QObject>
#include <QList>
#include <QString>
#include <QDebug>
#include <QObject>
#include <QHostInfo>
#include <QMutex>
#include "orb_interface.hh"
#include "dessine_module_communication_i.h"

class Connection : public QObject
{
    Q_OBJECT
public:
    Connection();
    Connection(int argc, char** argv);

    /*!
     * \brief start Start this connection
     */
    void start();
    /*!
     * \brief stop Stop this connection
     */
    void stop();

    /*!
     * \brief getIOR Get the IOR (Interoperable Object Reference), that is, an identifier for this connection.
     * \return
     */
    const QString getIOR() const;

    /*!
     * \brief getHostInfo Return an HostInfo object, holding the informations about this host
     * \return
     */
    dessine_module::HostInfo getHostInfo();
    /*!
     * \brief getInstance Return the istance of the implementation of the "Communication" CORBA interface
     * \return
     */
    const QSharedPointer<dessine_module_Communication_i> getInstance();

    /*!
     * \brief tickets Return a list of tickets avaible. Each image sent to the servant is associated with a ticket, that is used to get back from the servant the comments related to it.
     * \return
     */
    QList<QString> tickets();


    CORBA::String_var str_instance_reference;
    QString instanceReferenceString;


private:
    int argc;
    char** argv;
    QString ior;
    CORBA::ORB_var orb;
    QSharedPointer<dessine_module_Communication_i> instance;
    QList<QString> receivedTicket;
    QMutex mutex;





    void init();
    void onReadyForPull(const QString&, const dessine_module::HostInfo&);

signals:
    /*!
     * \brief connectionReady emitted when the connection is ready
     */
    void connectionReady() const;
    /*!
     * \brief connectionDestroyed emitted when the connection is destroyed
     */
    void connectionDestroyed() const;
    /*!
     * \brief readyForPull Signal emitted when the servant call the readyForPull method to the client
     */
    void readyForPull(const QString&, const dessine_module::HostInfo&);

};

#endif // CONNECTION_H
