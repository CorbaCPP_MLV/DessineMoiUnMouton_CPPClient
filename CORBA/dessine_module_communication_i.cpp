#include "dessine_module_communication_i.h"

dessine_module_Communication_i::dessine_module_Communication_i()
{ }
dessine_module_Communication_i::~dessine_module_Communication_i()
{ }

// Methods corresponding to IDL attributes and operations
void dessine_module_Communication_i::registerProducer(const dessine_module::HostInfo& info)
{ }

char* dessine_module_Communication_i::pushImage(const dessine_module::Image& img, const dessine_module::HostInfo& producerInfo)
{ }

void dessine_module_Communication_i::readyForPull(const char* ticket, const dessine_module::HostInfo& producerInfo)
{
    onReadyForPull(QString(ticket), producerInfo);
}

dessine_module::StringArray* dessine_module_Communication_i::pullComments(const char* ticket)
{ }

void dessine_module_Communication_i::disconnect(const dessine_module::HostInfo& info)
{ }
