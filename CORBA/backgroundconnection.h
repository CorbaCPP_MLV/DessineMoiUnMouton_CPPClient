#ifndef DESSINEWORKER_H
#define DESSINEWORKER_H

#include <QThread>
#include <QObject>
#include <QFile>
#include <QList>
#include <QString>
#include <QDebug>
#include <vector>
#include <QHostInfo>
#include "orb_interface.hh"
#include "connection.h"
#include "dessine_module_communication_i.h"

/*!
 * \brief The BackgroundConnection class is responsible for creating a corba connection to a host in a separate thread.
 */
class BackgroundConnection : public QThread
{
    Q_OBJECT

private:
    BackgroundConnection();

public:

    void run() Q_DECL_OVERRIDE;
    void stopServer();

    /*!
     * \brief getConnection Return a pointer to the connection
     * \return
     */
    const QSharedPointer<Connection> getConnection();

    /*!
     * \brief createConnection Static factory method for creating a BackgroundConnection.
     * \param argc
     * \param argv
     * \return
     */
    static QSharedPointer<BackgroundConnection> createConnection(int argc, char** argv)
    {
        QSharedPointer<BackgroundConnection> bg = QSharedPointer<BackgroundConnection>(new BackgroundConnection());
        bg->connection = QSharedPointer<Connection>(new Connection(argc, argv));

        bg->connect(bg->connection.data(), &Connection::connectionReady, [bg](void)
        {
            bg->emit connectionReady();
        });

        bg->connect(bg->connection.data(), &Connection::connectionDestroyed, [bg](void)
        {
            bg->emit connectionDestroyed();
        });

        bg->connect(bg->connection.data(), &Connection::readyForPull, [bg](const QString & ticket, const dessine_module::HostInfo & hostInfo)
        {
            bg->emit readyForPull(ticket, hostInfo);
        });

        return bg;
    }

private:
    QSharedPointer<Connection> connection;

signals:
    /*!
     * \brief connectionReady emitted when the connection is ready
     */
    void connectionReady() const;
    /*!
     * \brief connectionDestroyed emitted when the connection is destroyed
     */
    void connectionDestroyed() const;
    /*!
     * \brief readyForPull Signal emitted when the servant call the readyForPull method to the client
     */
    void readyForPull(const QString&, const dessine_module::HostInfo&);
};

#endif // DESSINEWORKER_H
