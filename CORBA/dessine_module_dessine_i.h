#ifndef DESSINE_MODULE_DESSINE_I_H
#define DESSINE_MODULE_DESSINE_I_H

#include "orb_interface.hh"

//
// Example class implementing IDL interface dessine_module::Dessine
//
class dessine_module_Dessine_i : public POA_dessine_module::Dessine {
private:
  // Make sure all instances are built on the heap by making the
  // destructor non-public
  //virtual ~dessine_module_Dessine_i();

public:
  // standard constructor
  dessine_module_Dessine_i();
  virtual ~dessine_module_Dessine_i();

  // methods corresponding to defined IDL attributes and operations
  ::CORBA::Boolean sendImage(const dessine_module::Image& img, const dessine_module::HostInfo& destination);
  ::CORBA::Boolean sendComments(const dessine_module::StringArray& comments, const dessine_module::HostInfo& destination);
  ::CORBA::Boolean sendImageWithComments(const dessine_module::Image& img, const dessine_module::StringArray& comments, const dessine_module::HostInfo& destination);
};

#endif // DESSINE_MODULE_DESSINE_I_H
