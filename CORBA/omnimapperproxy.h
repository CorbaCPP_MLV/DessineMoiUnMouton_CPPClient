#ifndef OMNIMAPPERPROXY_H
#define OMNIMAPPERPROXY_H

#include <QObject>
#include <QProcess>
#include <QFile>
#include <QTextStream>
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;

/*!
 * \brief The OmniMapperProxy class is a proxy for the omniMapper utility. After a connection has been created, this class can be used to make the local host to listen for remote request to be forwarded to the created connection.
 */
class OmniMapperProxy
{

public:
    OmniMapperProxy(QObject* parent): port("1234"), cfgName("/tmp/omniMapper.cfg")
    {
        process = new QProcess(parent);
    }

    /*!
     * \brief setIOR Set the IOR used to configure omniMapper
     * \param IOR
     */
    void setIOR(const QString& IOR)
    {
        this->ior = QString(IOR);
    }

    /*!
     * \brief setConfigFilename set the full-path to the file to be used as configuration file for omniMapper
     * \param fname
     */
    void setConfigFilename(const QString& fname)
    {
        this->cfgName = QString(fname);
    }

    void start()
    {
        //Create a new file
        QFile file(cfgName);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);

        //Append the content to the file
        QString str("DESSINE ");
        str.append(ior).append("\n ");
        out << str;
        file.close();

        //Execute omniMapper
        //QString command("omniMapper -port ");
        //command.append(port).append(" -config ").append(cfgName).append(" -v");
        //process->start(command);
        //QMessageLogger().info("omniMapper started!");


        //QString command("omniNames -port ");
        //command.append(port).append(" -config ").append(cfgName).append(" -v");
        //process->start(command);

    }

    void stop()
    {
        if(process != nullptr)
            process->kill();
    }

private:
    QProcess* process;
    QString ior;
    QString cfgName;
    QString port;
};

#endif // OMNIMAPPERPROXY_H
