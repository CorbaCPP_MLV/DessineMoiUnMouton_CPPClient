#include "commentsdialog.h"
#include "ui_commentsdialog.h"


CommentsDialog::CommentsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CommentsDialog)
{
    ui->setupUi(this);
    treeViewDataModel = QSharedPointer<QStandardItemModel>(new QStandardItemModel);
    treeViewDataModel->setHorizontalHeaderLabels(QStringList("Image"));
    ui->commentsTreeView->setModel(treeViewDataModel.data());

    renderArea = QSharedPointer<RenderArea>(new RenderArea);
    ui->scrollArea->setWidget(renderArea.data());
    ui->scrollArea->setWidgetResizable(false);
    renderArea->setFixedWidth(1);
    renderArea->setFixedHeight(1);
}

void CommentsDialog::setCommentsHolder(CommentsHolder* holder)
{
    this->holder = holder;
}


void CommentsDialog::getComments(const QList<QString>& tickets, const QString& address, const QString& port)
{
    //dessine_module::Communication_ptr serverInstance = ConnectionUtils::getConnection(address, port);

    /*QStringList allTickets = QStringList(tickets);
    allTickets.append(holder->getTickets());*/

    for(const auto& ticket : holder->getTickets())
    {
        /*
        QStringList comments = ConnectionUtils::getComments(serverInstance, ticket);

        QMessageLogger().info(QString("[INFO] Received: ").append(QString::number(comments.size())).append(" comments for ticket.").append(ticket).toUtf8().data());

        for(const auto& oldComment : holder->getComments(ticket))
        {
            if(!comments.contains(oldComment))
                comments.append(oldComment);
        }
*/


        QStandardItem* imageItem = new QStandardItem(QString("Ticket ").append(ticket));
        for(const auto& comment : holder->getComments(ticket))//comments)
        {
            imageItem->appendRow(new QStandardItem(comment));
        }
        treeViewDataModel->appendRow(imageItem);

        //holder->registerComments(ticket, comments);
    }
}

CommentsDialog::~CommentsDialog()
{
    delete ui;
}

void CommentsDialog::on_buttonBox_rejected()
{
    close();
}

void CommentsDialog::on_commentsTreeView_clicked(const QModelIndex &index)
{
    QStandardItem* selected =  treeViewDataModel->itemFromIndex(index);
    if(selected->parent())
    {
        selected = selected->parent();
    }

    QString ticket = selected->text().split(" ").last();
    QByteArray image = holder->getImage(ticket);

    QXmlStreamReader reader(image);
    try
    {
        renderArea->deserialize(reader);
    }
    catch(...)
    {
        QMessageLogger().critical(QString("[CRITICAL] Unable to deserialize image\n").append(image).toUtf8().data());
    }
}
