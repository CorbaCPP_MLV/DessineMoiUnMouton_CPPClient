<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="46"/>
        <source>Dessine Moi un Mouton!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="157"/>
        <source>Davide Andrea Guastella</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="202"/>
        <source>Daniele Greco</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="258"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ChooseLanguageDialog</name>
    <message>
        <location filename="chooselanguagedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Changer la langue</translation>
    </message>
    <message>
        <location filename="chooselanguagedialog.ui" line="28"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="chooselanguagedialog.ui" line="39"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
</context>
<context>
    <name>CommentsDialog</name>
    <message>
        <location filename="commentsdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Comments</translation>
    </message>
    <message>
        <location filename="commentsdialog.ui" line="30"/>
        <source>Image Preview</source>
        <translation>Avant-première</translation>
    </message>
    <message>
        <location filename="commentsdialog.ui" line="54"/>
        <source>Comments</source>
        <translation>Comments</translation>
    </message>
</context>
<context>
    <name>LocalServerConfigDialog</name>
    <message>
        <location filename="localserverconfigdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Configuration du Serveur</translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="28"/>
        <source>0000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="31"/>
        <source>1234</source>
        <translation></translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="38"/>
        <source>Listening Port</source>
        <translation>Port d&apos;écoute</translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="45"/>
        <source>Use local host address</source>
        <translation>Utilisèr l&apos;adresse locale</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Dessine Moi Un Mouton</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <source>Circle</source>
        <translation>Cercle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Rectangle</source>
        <translation>Rectangle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>Pen Color</source>
        <translation>Couleur du stylo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Draw a convex polygon. Left click for defining the points of the figure. Right click for drawing the polygon.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Dessiner un polygone convexe. Clic gauche pour définir les points de la figure. Clic droit pour dessiner le polygone</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <source>Polygon</source>
        <translation>Polygone</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>Ellipse</source>
        <translation>Ellipse</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="148"/>
        <source>Pen Size</source>
        <translation>Dimension du stylo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="155"/>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <source>Anti-Alias</source>
        <translation>Mode Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <source>Figures</source>
        <translation>Figures</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="210"/>
        <source>Undo Selection</source>
        <translation>Annuler la sélection</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <source>Remote</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="298"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source> New Image</source>
        <translation>Nouvelle Image</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Open Image</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="339"/>
        <location filename="mainwindow.cpp" line="454"/>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <source>Export as PNG</source>
        <translation>Exporter comme PNG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="362"/>
        <source>Save As</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="371"/>
        <source>Send to Server...</source>
        <translation>Envoyer au serveur...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <source>Start Server</source>
        <translation>Démarrer le Serveur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="389"/>
        <source>About</source>
        <translation>Á propos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Change language</source>
        <translation>Changer la langue</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <source>Local Server Configuration</source>
        <translation>Configuration du Serveur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <source>Comments</source>
        <translation>Comments</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="125"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <source>Sort By</source>
        <translation>Trier par</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="131"/>
        <source>Area</source>
        <translation>Area</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="132"/>
        <source>Area (Descending)</source>
        <translation>Area (Descendant)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="133"/>
        <source>Perimeter</source>
        <translation>Périmètre</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="134"/>
        <source>Perimeter (Descending)</source>
        <translation>Périmètre (Descendant)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="135"/>
        <source>Distance from Origin</source>
        <translation>Distance de l&apos;origine</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="136"/>
        <source>Distance from Origin (nearest)</source>
        <translation>Distance de l&apos;origine (plus proche)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="456"/>
        <source>PNG Image (*.png)</source>
        <translation>Image PNG (*.png)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="476"/>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Vectorial Image (*.xml)</source>
        <translation>Image vectoriel (*.xml)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <source>Dessine</source>
        <translation type="vanished">Dessine</translation>
    </message>
    <message>
        <source>Start server first!</source>
        <translation type="vanished">D&apos;abord, démarrer le serveur!</translation>
    </message>
    <message>
        <source>Server started correctly.</source>
        <translation type="vanished">Server démarré correctement.</translation>
    </message>
</context>
<context>
    <name>NewImageDialog</name>
    <message>
        <location filename="newimagedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Nouvelle Image</translation>
    </message>
    <message>
        <location filename="newimagedialog.ui" line="31"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="newimagedialog.ui" line="51"/>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
</context>
<context>
    <name>SendToServerDialog</name>
    <message>
        <location filename="sendtoserverdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Connection</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="30"/>
        <source>Server Address</source>
        <translation>Adresse du serveur</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="37"/>
        <source>000.000.000.000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="40"/>
        <source>127.0.0.1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="47"/>
        <source>Server Port</source>
        <translation>Porte</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="54"/>
        <source>9999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="57"/>
        <source>1234</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="96"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Connect to Server</source>
        <translation type="vanished">Connecte au serveur</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="89"/>
        <source>Send Image</source>
        <translation>Envoyer l&apos;image</translation>
    </message>
    <message>
        <source>Received comments</source>
        <translation type="vanished">Comments reçus</translation>
    </message>
    <message>
        <source>Ask For Comments</source>
        <translation type="vanished">Démander  les comments</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="41"/>
        <source>Image correctly sent to server.</source>
        <translation>Image envoyée au serveur.</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="54"/>
        <location filename="sendtoserverdialog.cpp" line="61"/>
        <location filename="sendtoserverdialog.cpp" line="79"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="55"/>
        <location filename="sendtoserverdialog.cpp" line="62"/>
        <source>Unable to contact server. Check the server status.</source>
        <translation>Incapable de contacter le serveur. Vérifier l&apos;état du serveur.</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="40"/>
        <source>Dessine</source>
        <translation>Dessine</translation>
    </message>
    <message>
        <source>No comments avaible!</source>
        <translation type="vanished">Pas de comments disponibles!</translation>
    </message>
    <message>
        <source>Connection established!</source>
        <translation type="vanished">Connection établie!</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="80"/>
        <source>Invalid IP address.</source>
        <translation>Adresse IP invalide.</translation>
    </message>
</context>
</TS>
