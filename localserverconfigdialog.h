#ifndef LOCALSERVERCONFIGDIALOG_H
#define LOCALSERVERCONFIGDIALOG_H

#include <QDialog>

namespace Ui {
class LocalServerConfigDialog;
}

class LocalServerConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LocalServerConfigDialog(QWidget *parent = 0);
    ~LocalServerConfigDialog();

    void setPort(const QString& val);
    void setUseLocalhost(const bool val);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

signals:
    void configured(const QString& port, const bool useLocalhost);

private:
    Ui::LocalServerConfigDialog *ui;
};

#endif // LOCALSERVERCONFIGDIALOG_H
