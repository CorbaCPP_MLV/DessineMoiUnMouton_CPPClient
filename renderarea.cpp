/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "renderarea.h"

RenderArea::RenderArea(QWidget *parent)
    : QWidget(parent)
{
    shape = Polygon;
    antialiased = false;
    transformed = false;
    setBackgroundRole(QPalette::Base);
    setMouseTracking(true);

    image = QSharedPointer<geometry::Image>(new geometry::Image());
    this->pen.setColor(QColor(0, 0, 0));
}

QSize RenderArea::minimumSizeHint() const
{
    return QSize(100, 100);
}

QSize RenderArea::sizeHint() const
{
    return QSize(400, 200);
}

void RenderArea::setShape(Shape shape)
{
    this->shape = shape;
    update();
}

void RenderArea::setPen(const QPen &pen)
{
    this->pen = pen;
    update();
}

void RenderArea::setBrush(const QBrush &brush)
{
    this->brush = brush;
    update();
}

void RenderArea::setAntialiased(bool antialiased)
{
    this->antialiased = antialiased;
    update();
}

void RenderArea::setTransformed(bool transformed)
{
    this->transformed = transformed;
    update();
}

void RenderArea::mousePressEvent(QMouseEvent *event)
{

    switch(shape)
    {
        case Line:
        case Rect:
        case Ellipse:
        case Circle:
            points.append(QPoint(event->x(), event->y()));
            break;
        case Points:
        case Polygon:
            break;
    }

    emit mousePressed(QPoint(event->x(), event->y()));
}

QSharedPointer<geometry::Image> RenderArea::getImage()
{
    return this->image;
}

void RenderArea::mouseReleaseEvent(QMouseEvent *event)
{
    int x = event->x();
    int y = event->y();
    QSharedPointer<geometry::Figure> newFigure;
    switch(shape)
    {
        case Line:
        {
            points.append(QPoint(x, y));
            newFigure = geometry::Ligne::createLine(points.first(), points.last(), QPen(this->pen), QBrush(this->brush));
        }
        break;

        case Rect:
        {
            points.append(QPoint(x, y));
            newFigure = geometry::Rectangle::createRectangle(points.first(), points.last(), QPen(this->pen), QBrush(this->brush));
        }
        break;

        case Ellipse:
        {
            points.append(QPoint(x, y));
            newFigure = geometry::Ellipse::createEllipse(points.first(), points.last(), QPen(this->pen), QBrush(this->brush));
        }
        break;
        case Circle:
        {
            QRect rect(QPoint(x, y), points.last());
            unsigned radius = abs(rect.height()) > abs(rect.width()) ? abs(rect.height()) : abs(rect.width());
            newFigure = geometry::Cercle::createCercle(rect.bottomRight(), abs(radius), QPen(this->pen), QBrush(this->brush));
        }
        break;
        case Polygon:
            if(event->button() == Qt::RightButton)
            {
                if(points.size() > 2)
                {
                    newFigure = geometry::ConvexPolygon::createConvexPolygon(points, QPen(this->pen), QBrush(this->brush));
                }
                else
                {
                    points.clear();
                    update();
                    return;
                }
            }
            else
            {
                points.append(QPoint(event->x(), event->y()));
                return;
            }
            break;
    }

    points.clear();

    int figureID = -1;
    if((figureID = image->ajouter(newFigure)) == -1)
        return;

    repaint();
    emit figureAdded(*newFigure.data(), figureID);
    emit mouseReleased(QPoint(event->x(), event->y()));
}

void RenderArea::paintEvent(QPaintEvent*/*event*/)
{
    QPainter painter(this);

    painter.setPen(pen);
    painter.setBrush(brush);
    if(antialiased)
        painter.setRenderHint(QPainter::Antialiasing, true);

    if(points.size() > 0 && shape == Polygon)
    {
        for(const auto& point : points)
        {
            painter.drawPoints(QPolygon(points));
        }
    }


    image->dessiner(painter);
}

QImage RenderArea::toImage() const
{
    QImage im(size(), QImage::Format_RGB888);
    im.fill(QColor(Qt::white).rgb());
    QPainter painter(&im);
    image->dessiner(painter);
    return im;
}

void RenderArea::removeFigure(const unsigned figureID)
{
    image->removeImage(figureID);
    QMessageLogger().info(QString("Figures count: ").append(QString::number(image->getNombre())).toUtf8().data());
    update();
}


void RenderArea::setPenSize(const unsigned w)
{
    pen.setWidth(w);
}

void RenderArea::setPenBrush(const QBrush& w)
{
    pen.setBrush(w);
}

const QColor RenderArea::getBrushColor()
{
    return brush.color();
}

const QColor RenderArea::getPenColor()
{
    return pen.color();
}

void RenderArea::setBrushColor(const QColor& c)
{
    brush.setColor(c);
}

void RenderArea::setPenColor(const QColor& c)
{
    pen.setColor(c);
}

void RenderArea::newImage()
{
    image = QSharedPointer<geometry::Image>(new geometry::Image());
    this->pen.setColor(QColor(0, 0, 0));
}

void RenderArea::highlightFigure(int figureID)
{
    image->highlightFigure(figureID);
    update();
}

void RenderArea::undoHighlight()
{
    image->undoHighlight();
    update();
}


void RenderArea::serialize(QXmlStreamWriter& writer)
{
    writer.writeStartElement("document");
    writer.writeAttribute("width", QString::number(width()));
    writer.writeAttribute("height", QString::number(height()));
    writer.writeAttribute("antialiased", QString::number(antialiased));
    image->serialize(writer);
    writer.writeEndElement();
}

void RenderArea::deserialize(QXmlStreamReader& reader)
{
    QString log("Loading Image ");

    //Start reading the document
    reader.readNext();

    //jump to "<document>" tag
    if(reader.isStartDocument())
        reader.readNextStartElement();

    if(!(reader.name() == "document"))
        return;

    //Iterate each figure
    foreach(const QXmlStreamAttribute &attr, reader.attributes())
    {
        if(attr.name().toString() == QLatin1String("width"))
        {
            setFixedWidth(attr.value().toUInt());
            log.append(attr.value());
        }
        else if(attr.name().toString() == QLatin1String("height"))
        {
            setFixedHeight(attr.value().toUInt());
            log.append("x").append(attr.value());
        }
        else if(attr.name().toString() == QLatin1String("antialiased"))
        {
            setAntialiased(attr.value().toInt());
        }
    }
    QMessageLogger().info(log.toUtf8().data());
    reader.readNextStartElement();
    image = geometry::Image::createImage(reader);

    update();

    for(const auto& addedFigID : image->getFigures().keys())
    {
        emit figureAdded(*image->getFigures()[addedFigID].data(), addedFigID);
    }
}
