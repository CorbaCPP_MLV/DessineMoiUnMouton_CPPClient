#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDebug>
#include <QColorDialog>
#include <QMetaEnum>
#include <QListWidgetItem>
#include <QFileDialog>
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include <QCloseEvent>
#include <QTranslator>
#include "renderarea.h"
#include "newimagedialog.h"
#include "sendtoserverdialog.h"
#include "CORBA/backgroundconnection.h"
#include "CORBA/omnimapperproxy.h"
#include "chooselanguagedialog.h"
#include "Network/tcpbackgroundconnection.h"
#include "localserverconfigdialog.h"
#include "commentsdialog.h"
#include "aboutdialog.h"
#include "geometrie/figure.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onMousePressEvent(QPoint p);
    void onMouseReleaseEvent(QPoint p);
    void on_ellipsePushButton_clicked();
    void on_linePushButton_clicked();
    void on_circlePushButton_clicked();
    void on_rectanglePushButton_clicked();
    void on_polygonPushButton_clicked();
    void on_brushColorPushButton_clicked();
    void on_spinBox_valueChanged(int arg1);
    void showContextMenu(const QPoint&);
    void on_FigureSelected(QListWidgetItem* item);
    void on_FigureAdded(geometry::Figure& f, int figureID);
    void removeFigure();
    void on_antiAliasCheckBox_clicked(bool checked);
    void on_deselectFiguresPushButton_clicked();
    void on_action_New_Image_triggered();
    void on_actionOpen_Image_triggered();
    void on_actionSave_triggered();
    void on_actionExit_triggered();
    void on_actionExport_as_PNG_triggered();
    void on_actionSave_As_triggered();
    void on_actionSend_to_Server_triggered();
    void on_actionAbout_triggered();
    void on_actionChange_language_triggered();

    void on_actionLocal_Server_Configuration_triggered();

    void on_actionComments_triggered();

    void sortFiguresByArea(bool ascending = true);
    void sortFiguresByPerimeter(bool ascending = true);
    void sortFiguresByDistanceFromOrigin(bool farther = true);
    void sortFigures(bool order, unsigned parameter);

private:
    void closeEvent(QCloseEvent */*event*/) Q_DECL_OVERRIDE;
    void quit();
    void setupRenderArea(QSharedPointer<RenderArea> r, const unsigned width = 800, const unsigned height = 600);
    bool askForSave();
    void exportImageAsPNG();
    void saveImage();
    void openImage();
    void saveToXML(QByteArray& byteArray);
    void saveToXML(QString& imageFileName);
    void changeLanguage(const QString& language);
    void setLanguage();
    void startBackgroundTCPServer(const QString& address, const QString& port);
    QString chooseServer();


    void receiveData(const QString& ticket);


private:
    /*!
     * \brief savedChanges A boolean value indicating wheter the image has been modified. This is used to warn the user about saving the image before exiting the application, creating a new image or opening another image.
     */
    bool savedChanges;

    /*!
     * \brief imageFileName The full-path file name of the current image
     */
    QString imageFileName;

    /*!
     * \brief renderArea The render area used for drawing
     */
    QSharedPointer<RenderArea> renderArea;

    /*!
     * \brief tcpServer A TCP server listening for remote requests. This is used to notify to this client that comments are avaible for the image.
     */
    TcpBackgroundConnection* tcpServer;

    /*!
     * \brief translator A translator object used to translate the UI
     */
    QTranslator* translator;

    /*!
     * \brief applicationLanguage The current language of the application
     */
    QString applicationLanguage;

    /*!
     * \brief registeredServer A list that keeps track of
     */
    QList<dessine_module::HostInfo> registeredServer;

    /*!
     * \brief useLocalhostAddress a boolean value indicating if the localhost address is used for listening to requests
     */
    bool useLocalhostAddress;

    /*!
     * \brief avaibleTickets
     */
    QList<QString> avaibleTickets;

    /*!
     * \brief hostInfo The informations about the current host
     */
    dessine_module::HostInfo hostInfo;


    CommentsHolder commentsHolder;

    Ui::MainWindow *ui;


protected:

    void changeEvent(QEvent*);
};



#endif // MAINWINDOW_H
