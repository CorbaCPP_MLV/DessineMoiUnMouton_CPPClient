<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="46"/>
        <source>Dessine Moi un Mouton!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="157"/>
        <source>Davide Andrea Guastella</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="202"/>
        <source>Daniele Greco</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="258"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ChooseLanguageDialog</name>
    <message>
        <location filename="chooselanguagedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Change language</translation>
    </message>
    <message>
        <location filename="chooselanguagedialog.ui" line="28"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="chooselanguagedialog.ui" line="39"/>
        <source>French</source>
        <translation>French</translation>
    </message>
</context>
<context>
    <name>CommentsDialog</name>
    <message>
        <location filename="commentsdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Comments</translation>
    </message>
    <message>
        <location filename="commentsdialog.ui" line="30"/>
        <source>Image Preview</source>
        <translation>Image Preview</translation>
    </message>
    <message>
        <location filename="commentsdialog.ui" line="54"/>
        <source>Comments</source>
        <translation>Comments</translation>
    </message>
</context>
<context>
    <name>LocalServerConfigDialog</name>
    <message>
        <location filename="localserverconfigdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Server Configuration</translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="28"/>
        <source>0000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="31"/>
        <source>1234</source>
        <translation></translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="38"/>
        <source>Listening Port</source>
        <translation>Listening Port</translation>
    </message>
    <message>
        <location filename="localserverconfigdialog.ui" line="45"/>
        <source>Use local host address</source>
        <translation>Use Local Host Address</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Dessine moi un mouton</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <source>Circle</source>
        <translation>Circle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Rectangle</source>
        <translation>Rectangle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>Pen Color</source>
        <translation>Pen color</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Draw a convex polygon. Left click for defining the points of the figure. Right click for drawing the polygon.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Draw a convex polygon. Left click for defining the points of the figure. Right click for drawing the polygon</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <source>Polygon</source>
        <translation>Polygon</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>Ellipse</source>
        <translation>Ellipse</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="148"/>
        <source>Pen Size</source>
        <translation>Pen Size</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="155"/>
        <source>Line</source>
        <translation>Line</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <source>Anti-Alias</source>
        <translation>Anti-Alias</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <source>Figures</source>
        <translation>Figures</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="210"/>
        <source>Undo Selection</source>
        <translation>Undo Selection</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <source>Remote</source>
        <translation>Remote</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="298"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source> New Image</source>
        <translation>New Image</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Open Image</source>
        <translation>Open Image</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="339"/>
        <location filename="mainwindow.cpp" line="454"/>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <source>Export as PNG</source>
        <translation>Export as PNG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="362"/>
        <source>Save As</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="371"/>
        <source>Send to Server...</source>
        <translation>Send to Server...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <source>Start Server</source>
        <translation>Start Server</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="389"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Change language</source>
        <translation>Change language</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <source>Local Server Configuration</source>
        <translation>Local Server Configuration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <source>Comments</source>
        <translation>Comments</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="125"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <source>Sort By</source>
        <translation>Sort By</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="131"/>
        <source>Area</source>
        <translation>Area</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="132"/>
        <source>Area (Descending)</source>
        <translation>Area (Descending)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="133"/>
        <source>Perimeter</source>
        <translation>Perimeter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="134"/>
        <source>Perimeter (Descending)</source>
        <translation>Perimeter (Descending)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="135"/>
        <source>Distance from Origin</source>
        <translation>Distance from Origin</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="136"/>
        <source>Distance from Origin (nearest)</source>
        <translation>Distance from Origin (nearest)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="456"/>
        <source>PNG Image (*.png)</source>
        <translation>Png Image (*.png)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="476"/>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Vectorial Image (*.xml)</source>
        <translation>Vectorial Image (*.xml)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <source>Dessine</source>
        <translation type="vanished">Dessine</translation>
    </message>
    <message>
        <source>Start server first!</source>
        <translation type="vanished">Start server first!</translation>
    </message>
    <message>
        <source>Server started correctly.</source>
        <translation type="vanished">Server started correctly.</translation>
    </message>
</context>
<context>
    <name>NewImageDialog</name>
    <message>
        <location filename="newimagedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>New Image</translation>
    </message>
    <message>
        <location filename="newimagedialog.ui" line="31"/>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="newimagedialog.ui" line="51"/>
        <source>Height</source>
        <translation>Height</translation>
    </message>
</context>
<context>
    <name>SendToServerDialog</name>
    <message>
        <location filename="sendtoserverdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Remote</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="30"/>
        <source>Server Address</source>
        <translation>Server Address</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="37"/>
        <source>000.000.000.000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="40"/>
        <source>127.0.0.1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="47"/>
        <source>Server Port</source>
        <translation>Server Port</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="54"/>
        <source>9999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="57"/>
        <source>1234</source>
        <translation></translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="96"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <source>Connect to Server</source>
        <translation type="vanished">Connect to Server</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.ui" line="89"/>
        <source>Send Image</source>
        <translation>Send Image</translation>
    </message>
    <message>
        <source>Received comments</source>
        <translation type="vanished">Received Comments</translation>
    </message>
    <message>
        <source>Ask For Comments</source>
        <translation type="vanished">Ask for comments</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="41"/>
        <source>Image correctly sent to server.</source>
        <translation>Image correctly sent to server.</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="54"/>
        <location filename="sendtoserverdialog.cpp" line="61"/>
        <location filename="sendtoserverdialog.cpp" line="79"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="55"/>
        <location filename="sendtoserverdialog.cpp" line="62"/>
        <source>Unable to contact server. Check the server status.</source>
        <translation>Unable to contact server. Check the server status.</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="40"/>
        <source>Dessine</source>
        <translation>Dessine</translation>
    </message>
    <message>
        <source>No comments avaible!</source>
        <translation type="vanished">No comments available!</translation>
    </message>
    <message>
        <source>Connection established!</source>
        <translation type="vanished">Connection established!</translation>
    </message>
    <message>
        <location filename="sendtoserverdialog.cpp" line="80"/>
        <source>Invalid IP address.</source>
        <translation>Invalid IP address.</translation>
    </message>
</context>
</TS>
